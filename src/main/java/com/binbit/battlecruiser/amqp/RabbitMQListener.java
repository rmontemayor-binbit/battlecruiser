package com.binbit.battlecruiser.amqp;

import java.util.Date;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "Send_Kannel_Message")
public class RabbitMQListener {

    @RabbitHandler
    public void process(@Payload byte[] foo) {
        System.out.println(new Date() + ": " + new String(foo));
    }

}
