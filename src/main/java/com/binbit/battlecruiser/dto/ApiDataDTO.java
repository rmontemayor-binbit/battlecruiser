package com.binbit.battlecruiser.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ApiDataDTO {

    private Long id;
    private Long statusId;
    private String statusMessage;
    private BigDecimal credits;

}
