package com.binbit.battlecruiser.dto.kannel;

import lombok.Data;

/**
 * Bean that hold the values from a Kannel dlr request.
 *
 * @author rmontemayor
 *
 */
@Data
public class DeliveryReportDTO {
    /**
     * The idSmsc.
     */
    private String idSmsc;
    /**
     * The msisdn.
     */
    private String msisdn;

}
