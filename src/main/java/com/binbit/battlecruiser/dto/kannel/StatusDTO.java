package com.binbit.battlecruiser.dto.kannel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The status object for kannel responses.
 *
 * @author rmontemayor
 *
 */
@XmlRootElement(name = "status")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusDTO {
    /**
     * The code of the status.
     */
    @XmlAttribute
    private String code;

    /**
     * The value of the status.
     */
    @XmlValue
    private String value;
}
