package com.binbit.battlecruiser.dto.kannel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Bean representation of Kannel's Sms Response.
 *
 * @author rmontemayor
 *
 */
@XmlRootElement(name = "sms-confirmation")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmsResponseDTO {
    /**
     * The status of this object representation.
     */
    @XmlElement
    private StatusDTO status;
}
