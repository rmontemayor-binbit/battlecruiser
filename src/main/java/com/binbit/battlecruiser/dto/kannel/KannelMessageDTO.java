package com.binbit.battlecruiser.dto.kannel;

import lombok.Data;

/**
 * Bean for storage the params required for send a Kannel Message.
 *
 * @author rmontemayor
 *
 */
@Data
public class KannelMessageDTO {
    /**
     * Phone number who sends the message.
     */
    private String from;
    /**
     * Phone number who recives the message.
     */
    private String to;
    /**
     * Message to send.
     */
    private String text;
    /**
     * Defines a default DLR event bit mask which is used in combination with
     * the dlr-url config directive, if no specific X-Kannel HTTP header
     * parameter is returned by the HTTP response.
     *
     * @see Kannel Documentation http://bit.ly/2tSJ4BH
     */
    private Integer deliveryReportMask;
    /**
     * Defines a default URL which is fetched for DLR event, if no specific
     * X-Kannel HTTP header parameter is returned by the HTTP response.
     *
     * @see Kannel Documentation http://bit.ly/2tSJ4BH
     */
    private String deliveryReportUrl;
}
