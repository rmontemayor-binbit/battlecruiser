package com.binbit.battlecruiser.dto.kannel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionRequestDTO {
    private String shortCode;
    private String msisdn;
    private String text;
    private byte[] textbin;
    private String smscId;
    private String internalSmscId;
    private String delivery;
    private String coding;
    private String mclass;
    private String userDataHeader;
    private String charset;
    private String billing;
    private String osmsc;
    private String metadata;

    private String keyword;
}
