package com.binbit.battlecruiser.dto;

import lombok.Data;

@Data
public class ApiResponseDTO {
    private String status;
    private Long statusCode;
    private String message;
    private ApiDataDTO data;
}
