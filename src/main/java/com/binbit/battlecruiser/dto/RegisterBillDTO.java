package com.binbit.battlecruiser.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class RegisterBillDTO {

    private Long serviceId;
    private String msisdn;
    private BigDecimal amount;
    private String billingCode;
    private Integer shortCode;
    private Long carrierId;
    private String channel;
    private Long mediaId;
    private Long publisherId;
    private Long networkId;
    private Long promoId;

    private Long errorCode;
    private String errorMessage;
    private String carrierErrorCode;
    private String carrierErrorMessage;
}
