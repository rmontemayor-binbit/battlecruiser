package com.binbit.battlecruiser.dto;

import lombok.Data;

@Data
public class SubscriptionStatusParamDTO {

    private Long serviceId;
    private String msisdn;

}
