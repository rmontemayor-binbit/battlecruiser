/**
 * Package for DTOs related to promos application.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser.dto.promos;
