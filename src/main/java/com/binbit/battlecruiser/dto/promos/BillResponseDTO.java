package com.binbit.battlecruiser.dto.promos;

import lombok.Data;

/**
 * Bean representation of promos' bill parameter.
 *
 * @author rmontemayor
 *
 */
@Data
public class BillResponseDTO {
    /**
     * The code of the bill to a list of accepted values @see TODO .
     */
    private String code;
    /**
     * The message of the bill to a list of accepted values @see TODO .
     */
    private String message;
}
