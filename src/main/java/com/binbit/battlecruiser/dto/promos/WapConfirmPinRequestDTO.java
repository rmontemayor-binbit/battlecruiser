package com.binbit.battlecruiser.dto.promos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class WapConfirmPinRequestDTO extends WapRequestDTO {

    private String msisdn;
    private Long carrierId;
    private Long serviceId;
    private Long serviceTypeId;
    private Long promoId;
    private Long channelId;
    private String pin;
    private Long attemptId;
}
