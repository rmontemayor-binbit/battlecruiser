package com.binbit.battlecruiser.dto.promos;

import lombok.Data;

@Data
public class WapRequestDTO {

    private String trial;
    private String msisdn;
    private Long serviceId;
    private Long promoId;
    private Long attemptId;
    private Long publisherId;
    private String ipRemote;
    private Long carrierId;
    private Long serviceTypeId;
    private Long channelId;
    private String pin;

}
