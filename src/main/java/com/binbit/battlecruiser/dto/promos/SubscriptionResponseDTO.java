package com.binbit.battlecruiser.dto.promos;

import lombok.Data;

/**
 * Bean representation of promos application response subscription.
 *
 * @author rmontemayor
 *
 */
@Data
public class SubscriptionResponseDTO {
    /**
     * The code TODO @see.
     */
    private String code;
    /**
     * The message TODO @see.
     */
    private String message;
}
