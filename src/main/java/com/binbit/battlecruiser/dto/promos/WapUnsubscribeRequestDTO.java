package com.binbit.battlecruiser.dto.promos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Transport Bean for Promos Request for subscription
 *
 * @author rmontemayor
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class WapUnsubscribeRequestDTO extends WapRequestDTO {

    private String trial;
    private String msisdn;
    private Long serviceId;
    private Long promoId;
    private Long attemptId;
    private Long publisherId;
    private String ipRemote;

}
