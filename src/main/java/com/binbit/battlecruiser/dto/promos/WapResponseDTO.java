package com.binbit.battlecruiser.dto.promos;

import lombok.Data;

/**
 * Bean representation of promos response.
 *
 * @author rmontemayor
 *
 */
@Data
public class WapResponseDTO {
    /**
     * The subscription of this response.
     */
    private SubscriptionResponseDTO subscription;
    /**
     * The bill field of this response.
     */
    private BillResponseDTO bill;
    /**
     * String url for redirection purpose.
     */
    private String redirection;
}
