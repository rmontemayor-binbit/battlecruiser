package com.binbit.battlecruiser.dto.promos;

public class WapResponseBuilder {

    private final WapResponseDTO response;

    public WapResponseBuilder() {
        response = new WapResponseDTO();
        response.setBill(new BillResponseDTO());
        response.setSubscription(new SubscriptionResponseDTO());
    }

    public WapResponseBuilder setBillCode(String code) {
        response.getBill().setCode(code);
        return this;
    }

    public WapResponseBuilder setBillMessage(String message) {
        response.getBill().setMessage(message);
        return this;
    }

    public WapResponseBuilder setSubscriptionCode(String code) {
        response.getSubscription().setCode(code);
        return this;
    }

    public WapResponseBuilder setSubscriptionMessage(String message) {
        response.getSubscription().setMessage(message);
        return this;
    }

    public WapResponseBuilder setSubscriptionAndBillCode(String code) {
        setSubscriptionCode(code);
        setBillCode(code);
        return this;
    }

    public WapResponseBuilder setSubscriptionAndBillMessage(String message) {
        setSubscriptionMessage(message);
        setBillMessage(message);
        return this;
    }


    public WapResponseDTO build() {
        return response;
    }

}
