package com.binbit.battlecruiser.dto;

import java.util.Date;

import lombok.Data;

@Data
public class ChangeNextBillDateDTO {

    public Long serviceId;
    public String msisdn;
    public Date nextBillDate;
}
