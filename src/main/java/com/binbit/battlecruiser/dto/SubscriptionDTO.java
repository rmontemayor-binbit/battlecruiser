package com.binbit.battlecruiser.dto;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.binbit.battlecruiser.util.Constants;

import lombok.Data;

@Data
public class SubscriptionDTO {
    private Long serviceId = Constants.ZERO_LONG;
    private String msisdn = StringUtils.EMPTY;
    private Integer shortcode = Constants.ZERO_INT;
    private String keyword = StringUtils.EMPTY;
    private Long carrierId = Constants.ZERO_LONG;
    private Long mediaId = Constants.ZERO_LONG;
    private Long publisherId = Constants.ZERO_LONG;
    private Long networkId = Constants.ZERO_LONG;
    private Long promoId = Constants.ZERO_LONG;
    private String channel = StringUtils.EMPTY;
    private String externalServiceId = StringUtils.EMPTY;
    private String externalSubServiceId = StringUtils.EMPTY;
    private String externalSubscriptionId = StringUtils.EMPTY;
    private String priceCode = StringUtils.EMPTY;
    private Date nextBillDate = Constants.EPOCH_DATE;
}
