package com.binbit.battlecruiser.dto;

import lombok.Data;

@Data
public class SubscriptionCancelDTO {

    private Long serviceId;
    private String msisdn;
    private Integer shortcode;
    private String keyword;
    private Long carrierId;
    private String channel;
}
