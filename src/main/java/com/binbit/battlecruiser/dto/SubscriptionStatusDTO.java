package com.binbit.battlecruiser.dto;

import org.apache.commons.lang.StringUtils;

import com.binbit.battlecruiser.util.Constants;

import lombok.Data;

@Data
public class SubscriptionStatusDTO {
    private Long statusId = Constants.ZERO_LONG;
    private String statusMessage = StringUtils.EMPTY;
}
