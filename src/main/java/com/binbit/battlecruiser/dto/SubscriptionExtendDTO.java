package com.binbit.battlecruiser.dto;

import java.util.Date;

import lombok.Data;

@Data
public class SubscriptionExtendDTO {
    private Long serviceId;
    private String msisdn;
    private Date nextBillDate;
    private Integer credits;
}
