/**
 * Package for controllers related with promos application.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser.controller.promos;
