package com.binbit.battlecruiser.controller.promos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.binbit.battlecruiser.dto.promos.WapConfirmPinRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapResponseDTO;
import com.binbit.battlecruiser.dto.promos.WapSendPinRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapSubscribeRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapUnsubscribeRequestDTO;
import com.binbit.battlecruiser.service.WapService;

import lombok.Getter;
import lombok.Setter;

/**
 * Controller interface for promos.
 *
 * @author rmontemayor
 *
 */
@RestController
@RequestMapping("/wap")
@Getter
@Setter
public class WapController {

    /** Log de PromosWapController. */
    private static final Logger LOG = LoggerFactory.getLogger(WapController.class);

    @Autowired
    private WapService wapService;

    /**
     * Exposes the /wap/subscribe request for subscribe information.
     *
     * @param trial
     *            The trial of promos.
     * @param msisdn
     *            The user DN.
     * @param idService
     *            The promos application internal id generated when the service
     *            registered.
     * @param idPromo
     *            The promos application internal id generated when the promo
     *            registered.
     * @param att
     *            The internal promos' application of this transaction.
     * @param idPublisher
     *            The promos application internal id generated when the
     *            published registered.
     * @param ipRemote
     *            The remote ip of the user.
     * @return json representation of
     *         com.binbit.battlecruiser.dto.promos.PromosResponseDTO.
     */
    @RequestMapping(value = "/subscribe", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public WapResponseDTO subscribe(
            @RequestParam(required = false) final String trial,
            @RequestParam(required = false) final String msisdn,
            @RequestParam(required = false) final Long idService,
            @RequestParam(required = false) final Long idPromo,
            @RequestParam(required = false, name = "att") final Long idAttempt,
            @RequestParam(required = false) final Long idPublisher,
            @RequestParam(required = false, name = "ip_remote") final String ipRemote
            ) {
        final WapSubscribeRequestDTO request = new WapSubscribeRequestDTO(trial, msisdn, idService, idPromo, idAttempt, idPublisher, ipRemote);
        LOG.debug("subscribe() -- {}", request);
        return wapService.saveAttemptValidateSubscribeAndBill(request);
    }

    /**
     * Exposes the /wap/unsubscribe request for subscribe information.
     *
     * @param trial
     *            The trial of promos.
     * @param msisdn
     *            The user DN.
     * @param idService
     *            The promos application internal id generated when the service
     *            registered.
     * @param idPromo
     *            The promos application internal id generated when the promo
     *            registered.
     * @param att
     *            The internal promos' application of this transaction.
     * @param idPublisher
     *            The promos application internal id generated when the
     *            published registered.
     * @param ipRemote
     *            The remote ip of the user.
     * @return json representation of
     *         com.binbit.battlecruiser.dto.promos.PromosResponseDTO.
     */
    @RequestMapping(value = "/unsubscribe", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public WapResponseDTO unsubscribe(
            @RequestParam(required = false) final String trial,
            @RequestParam(required = false) final String msisdn,
            @RequestParam(required = false) final Long idService,
            @RequestParam(required = false) final Long idPromo,
            @RequestParam(required = false, name = "att") final Long idAttempt,
            @RequestParam(required = false) final Long idPublisher,
            @RequestParam(required = false, name = "ip_remote") final String ipRemote
            ) {
        final WapUnsubscribeRequestDTO request = new WapUnsubscribeRequestDTO(trial, msisdn, idService, idPromo, idAttempt, idPublisher, ipRemote);
        LOG.debug("unsubscribe() -- {}", request);
        return wapService.unsubscribe(request);
    }

    /**
     * Exposes the /wap/send_pin request for promos send pin registration.
     *
     * @param msisdn
     *            The user DN.
     * @param idCarrier
     *            The promos application internal id generated when the carrier
     *            registered.
     * @param idService
     *            The promos application internal id generated when the service
     *            registered.
     * @param idServiceType
     *            The promos application internal id generated when the service
     *            registered.
     * @param idPromo
     *            The promos application internal id generated when the promo
     *            registered.
     * @param idChannel
     *            The publisher id_channel.
     * @param pin
     *            Pin generated (promos always will created a pin, if carrier
     *            generate the pin just ignore this parameter).
     * @param idAttempt
     *            attempt_subscription id.
     * @return json representation of
     *         com.binbit.battlecruiser.dto.promos.PromosResponseDTO.
     */
    @RequestMapping(value = "/send_pin", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public WapResponseDTO sendPin(
            @RequestParam(required = false) final String msisdn,
            @RequestParam(required = false, name = "id_carrier") final Long idCarrier,
            @RequestParam(required = false, name = "id_service") final Long idService,
            @RequestParam(required = false, name = "id_service_type") final Long idServiceType,
            @RequestParam(required = false, name = "id_promo") final Long idPromo,
            @RequestParam(required = false, name = "id_channel") final Long idChannel,
            @RequestParam(required = false) final String pin,
            @RequestParam(required = false, name = "id_attempt") final Long idAttempt
            ) {
        final WapSendPinRequestDTO request = new WapSendPinRequestDTO(msisdn, idCarrier, idService, idServiceType, idPromo, idChannel, pin, idAttempt);
        LOG.debug("sendPin() -- {}", request);
        return wapService.sendPin(request);
    }

    /**
     * Exposes the /wap/confirm_pin request for promos send pin confirmation.
     *
     * @param msisdn
     *            The user DN.
     * @param idCarrier
     *            The promos application internal id generated when the carrier
     *            registered.
     * @param idService
     *            The promos application internal id generated when the service
     *            registered.
     * @param idServiceType
     *            The promos application internal id generated when the service
     *            registered.
     * @param idPromo
     *            The promos application internal id generated when the promo
     *            registered.
     * @param idChannel
     *            The publisher id_channel.
     * @param pin
     *            Pin generated (promos always will created a pin, if carrier
     *            generate the pin just ignore this parameter).
     * @param idAttempt
     *            attempt_subscription id.
     * @return json representation of
     *         com.binbit.battlecruiser.dto.promos.PromosResponseDTO.
     */
    @RequestMapping(value = "/confirm_pin", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public WapResponseDTO confirmPin(
            @RequestParam(required = false) final String msisdn,
            @RequestParam(required = false, name = "id_carrier") final Long idCarrier,
            @RequestParam(required = false, name = "id_service") final Long idService,
            @RequestParam(required = false, name = "id_service_type") final Long idServiceType,
            @RequestParam(required = false, name = "id_promo") final Long idPromo,
            @RequestParam(required = false, name = "id_channel") final Long idChannel,
            @RequestParam(required = false) final String pin,
            @RequestParam(required = false, name = "id_attempt") final Long idAttempt
             ) {
        final WapConfirmPinRequestDTO request = new WapConfirmPinRequestDTO(msisdn, idCarrier, idService, idServiceType, idPromo, idChannel, pin, idAttempt);
        LOG.debug("confirmPin() -- {}", request);
        return wapService.confirmPinAndSubscribe(request);
    }

}
