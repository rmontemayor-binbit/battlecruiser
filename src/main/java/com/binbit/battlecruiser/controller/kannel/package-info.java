/**
 * Package for controllers related to kannel interaction.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser.controller.kannel;
