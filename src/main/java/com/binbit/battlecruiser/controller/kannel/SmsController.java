package com.binbit.battlecruiser.controller.kannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.binbit.battlecruiser.dto.kannel.SmsResponseDTO;
import com.binbit.battlecruiser.dto.kannel.SubscriptionRequestDTO;
import com.binbit.battlecruiser.service.DeliveryReportService;
import com.binbit.battlecruiser.service.KannelService;
import com.binbit.battlecruiser.service.SmsService;

import lombok.Getter;
import lombok.Setter;

/**
 * Controller for kannel's sms reception.
 * @author rmontemayor
 *
 */
@RestController
@RequestMapping("/sms")
@Getter
@Setter
public class SmsController {

    /** SmsBoxController's log. */
    private static final Logger LOG = LoggerFactory.getLogger(SmsController.class);

    @Autowired
    private SmsService smsService;
    @Autowired
    private DeliveryReportService deliveryReportService;


    /**
     * Messages from kannel arrives here.
     *
     * @param shortCode
     *            Binbit's short code phone number.
     * @param msisdn
     *            User's DN.
     * @param text
     *            Text message from the user.
     * @param textbin
     *            Same as text but in bin format.
     * @param smscId
     *            Smsc name.
     * @param internalSmscId
     *            Kannel Smsc's id.
     * @param delivery
     *            The delivery report value.
     * @param coding
     *            Actually I dont know.
     * @param mclass
     *            Actually I dont know.
     * @param userDataHeader
     *            User Date Header of incoming message.
     * @param charset
     *            Message charset: for a "normal" message, it will be "GSM"
     *            (coding=1), "binary" (coding=2) or "UTF16-BE" (coding=3). If
     *            the message was successfully recoded from Unicode, it will be
     *            "ISO-8859-1".
     * @param billing
     *            Billing identifier/information of incoming message. The value
     *            depends on the SMSC module and the associated billing
     *            semantics of the specific SMSC providing the information. For
     *            EMI2 the value is the XSer 0c field, for SMPP it is the
     *            service_type of the deliver_sm PDU. (Note: This is used for
     *            proxying billing information to external applications. There
     *            is no semantics associated while processing these.)
     * @param osmsc
     *            Actually I dont know.
     * @param metadata
     *            Actually I dont know.
     * @return XML representation of SmsResponseDTO
     * @see Kanel documentation http://bit.ly/2tSJ4BH
     */
    @RequestMapping(value = "/kannel", produces = MediaType.APPLICATION_XML_VALUE)
    public SmsResponseDTO kannel(
            @RequestParam(required = false, name = "sc") final String shortCode,
            @RequestParam(required = false) final String msisdn,
            @RequestParam(required = false) final String text,
            @RequestParam(required = false) final byte[] textbin,
            @RequestParam(required = false, name = "smsc") final String smscId,
            @RequestParam(required = false, name = "id_smsc") final String internalSmscId,
            @RequestParam(required = false) final String delivery,
            @RequestParam(required = false) final String coding,
            @RequestParam(required = false) final String mclass,
            @RequestParam(required = false, name = "udh") final String userDataHeader,
            @RequestParam(required = false) final String charset,
            @RequestParam(required = false) final String billing,
            @RequestParam(required = false) final String osmsc,
            @RequestParam(required = false, name = "meta-data") final String metadata,
            @RequestParam(required = false) final String event) {
        LOG.debug("kannel() -- Request: /sms/kannel [shortCode[{}] msisdn[{}] text[{}] textbin[{}] smscId[{}] "
                        + "internalSmscId[{}] delivery[{}] coding[{}] mclass[{}] userDataHeader[{}] charset[{}] billing[{}] "
                        + "osmsc[{}] metadata[{}] event[{}]",
                shortCode, msisdn, text, textbin, smscId, internalSmscId, delivery, coding, mclass, userDataHeader, charset,
                billing, osmsc, metadata, event);
        try {
            final SubscriptionRequestDTO request = new SubscriptionRequestDTO(shortCode, msisdn, text, textbin, smscId,
                    internalSmscId, delivery, coding, mclass, userDataHeader, charset, billing, osmsc, metadata, null);
            smsService.process(request);
        } catch(final Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return KannelService.SUCCESS_KANNEL_RESPONSE;
    }

    /**
     * Request /sms/dlr for kannel dlr interaction.
     *
     * @param id
     *            The id of the attempt.
     * @param type
     *            When you deliver SMS to Kannel you have to indicate what kind
     *            of delivery report messages you would like to receive back
     *            from the system. The delivery report types currently
     *            implemented are:
     *            <ul>
     *            <li>1: delivery success</li>
     *            <li>2: delivery failure</li>
     *            <li>4: message buffered</li>
     *            <li>8: smsc submit</li>
     *            <li>16: smsc reject</li>
     *            <li>32: smsc intermediate notifications</li>
     *            </ul>
     * @return Xml representation of com.binbit.battlecruiser.dto.kannel.SmsResponseDTO
     */
    @RequestMapping(value = "/dlr", produces = MediaType.APPLICATION_XML_VALUE)
    public SmsResponseDTO dlr(
            @RequestParam(required = false) final Long id
            ,@RequestParam(required = false) final Integer type
            ) {
        LOG.debug("dlr() -- id[{}] type[{}]", id, type);
        deliveryReportService.process(id, type);
        return KannelService.SUCCESS_KANNEL_RESPONSE;
    }
}
