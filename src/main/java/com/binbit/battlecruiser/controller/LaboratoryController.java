package com.binbit.battlecruiser.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.binbit.battlecruiser.dao.kannel.SendSmsMapper;
import com.binbit.battlecruiser.service.KannelService;
import com.binbit.battlecruiser.service.SubscriptionService;
import com.binbit.battlecruiser.service.model.AttemptService;
import com.binbit.battlecruiser.service.model.ConfigurationService;
import com.binbit.battlecruiser.service.model.KeywordService;
import com.binbit.battlecruiser.service.model.ServiceModelService;
import com.binbit.battlecruiser.service.model.kannel.SendSmsService;

import lombok.Getter;
import lombok.Setter;

/**
 * Controller for test and debug purpose, shuldn't be versionated.
 * @author rmontemayor
 *
 */
@RestController
@Getter
@Setter
public class LaboratoryController {

    /** LaboratoryController's log. */
    private static final Logger LOG = LoggerFactory.getLogger(LaboratoryController.class);
    /**
     * KannelService.
     */
    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private AttemptService attemptService;

    @Autowired
    private KannelService kannelService;

    @Autowired
    private SendSmsMapper sendSmsMapper;

    @Autowired
    private SendSmsService sendSmsService;

    @Autowired
    private KeywordService keywordService;

    @Autowired
    private ServiceModelService serviceModelService;

    /**
     * To test.
     * @return Test to test
     */
    @RequestMapping("/test")
    public String test(@RequestParam String idSmsc, @RequestParam String shortCode, @RequestParam String text) {

        /*final SubscriptionRequestDTO request = new SubscriptionRequestDTO();
        request.setIdSmsc(idSmsc);
        request.setShortCode(shortCode);
        request.setText(text);
        serviceModelService.getService(request);*/
        return "true";
    }

    @RequestMapping("/test2")
    public String test2() {
        System.out.println(attemptService.get(1522L));

        return "OK";

    }




}
