package com.binbit.battlecruiser.dao.mapper;

import com.binbit.battlecruiser.model.KeywordType;
import com.binbit.battlecruiser.model.KeywordTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

@Mapper
public interface KeywordTypeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @SelectProvider(type=KeywordTypeSqlProvider.class, method="countByExample")
    long countByExample(KeywordTypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @DeleteProvider(type=KeywordTypeSqlProvider.class, method="deleteByExample")
    int deleteByExample(KeywordTypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @Delete({
        "delete from keyword_type",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @Insert({
        "insert into keyword_type (id, created_by, ",
        "created_date, updated_by, ",
        "updated_date, active, ",
        "name)",
        "values (#{id,jdbcType=BIGINT}, #{createdBy,jdbcType=VARCHAR}, ",
        "#{createdDate,jdbcType=TIMESTAMP}, #{updatedBy,jdbcType=VARCHAR}, ",
        "#{updatedDate,jdbcType=TIMESTAMP}, #{active,jdbcType=BIT}, ",
        "#{name,jdbcType=VARCHAR})"
    })
    @SelectKey(statement="select nextval('keyword_type_id_seq')", keyProperty="id", before=true, resultType=Long.class)
    int insert(KeywordType record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @InsertProvider(type=KeywordTypeSqlProvider.class, method="insertSelective")
    @SelectKey(statement="select nextval('keyword_type_id_seq')", keyProperty="id", before=true, resultType=Long.class)
    int insertSelective(KeywordType record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @SelectProvider(type=KeywordTypeSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="created_by", property="createdBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="created_date", property="createdDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="updated_by", property="updatedBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="updated_date", property="updatedDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="active", property="active", jdbcType=JdbcType.BIT),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR)
    })
    List<KeywordType> selectByExample(KeywordTypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @Select({
        "select",
        "id, created_by, created_date, updated_by, updated_date, active, name",
        "from keyword_type",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="created_by", property="createdBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="created_date", property="createdDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="updated_by", property="updatedBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="updated_date", property="updatedDate", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="active", property="active", jdbcType=JdbcType.BIT),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR)
    })
    KeywordType selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @UpdateProvider(type=KeywordTypeSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") KeywordType record, @Param("example") KeywordTypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @UpdateProvider(type=KeywordTypeSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") KeywordType record, @Param("example") KeywordTypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @UpdateProvider(type=KeywordTypeSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(KeywordType record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table keyword_type
     *
     * @mbg.generated
     */
    @Update({
        "update keyword_type",
        "set created_by = #{createdBy,jdbcType=VARCHAR},",
          "created_date = #{createdDate,jdbcType=TIMESTAMP},",
          "updated_by = #{updatedBy,jdbcType=VARCHAR},",
          "updated_date = #{updatedDate,jdbcType=TIMESTAMP},",
          "active = #{active,jdbcType=BIT},",
          "name = #{name,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(KeywordType record);
}