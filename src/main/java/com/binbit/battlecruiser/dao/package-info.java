/**
 *
 * Base package for mybatis Mappers and SqlProviders.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser.dao;
