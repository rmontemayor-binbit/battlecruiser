package com.binbit.battlecruiser.model.kannel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SendSmsWithBLOBs extends SendSms {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column send_sms.udhdata
     *
     * @mbg.generated
     */
    private byte[] udhdata;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column send_sms.meta_data
     *
     * @mbg.generated
     */
    private String metaData;
}