package com.binbit.battlecruiser.model;

public interface LogicalDeletable {

    public Boolean getActive();

    public void setActive(Boolean active);
}
