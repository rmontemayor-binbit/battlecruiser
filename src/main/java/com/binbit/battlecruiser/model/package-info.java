/**
 *
 * Base package for table models and mybatis Example.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser.model;
