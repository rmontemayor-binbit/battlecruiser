package com.binbit.battlecruiser.util;

import java.util.Date;

public class Constants {

    public static final String SUBSCRIPTION_REGISTER_PATH = "/subscription/register";

    public static final String SUBSCRIPTION_CANCEL_PATH = "/subscription/cancel";

    public static final String SUBSCRIPTION_EXTEND_PATH = "/subscription/extend";

    public static final String SUBSCRIPTION_CHANGE_NEXT_BILL_DATE_PATH = "/subscription/changeNextBillDate";

    public static final String SUBSCRIPTION_ADD_GRACE_PERIOD_IN_DAYS_PATH = "/subscription/addGracePeriodInDays";

    public static final String SUBSCRIPTION_ADD_GRACE_PERIOD_PATH = "/subscription/addGracePeriodInDate";

    public static final String SUBSCRIPTION_REGISTER_BILL_PATH = "/subscription/registerBill";

    public static final String SUBSCRIPTION_REGISTER_FAILED_BILL_PATH = "/subscription/registerFailedBill";

    public static final String SUBSCRIPTION_REGISTER_DOWNLOAD_PATH = "/subscription/registerDownload";

    public static final String SUBSCRIPTION_REGISTER_MOBILE_ORIGINATED_PATH = "/subscription/registerMo";

    public static final String SUBSCRIPTION_REGISTER_MOBILE_TERMINATED_PATH = "/subscription/registerMt";

    public static final String SUBSCRIPTION_DEBIT_CREDITS_PATH = "/subscription/debitCredits";

    public static final String SUBSCRIPTION_ADD_CREDITS_PATH = "/subscription/addCredits";

    public static final String SUBSCRIPTION_STATUS_PATH = "/subscription/info";

    public static final String SUBSCRIPTION_BLACKLIST_PATH = "/subscription/blacklist";

    public static final Long ZERO_LONG = Long.valueOf(0l);

    public static final Integer ZERO_INT = Integer.valueOf(0);

    public static final Date EPOCH_DATE = new Date(0);

    public static final String SUCCESS = "success";

}
