package com.binbit.battlecruiser.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class Utils {

    /**
     * Fail-safe toString converter
     *
     * @return empty String if null or the object as string
     */
    public static String toString(Object o) {
        return Objects.toString(o, StringUtils.EMPTY);
    }

    public static String toString(BigDecimal o) {
        return o == null ? StringUtils.EMPTY : o.toPlainString();
    }

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static String toString(Date o) {
        return o == null ? StringUtils.EMPTY : sdf.format(o);
    }

    public static Long toLong(String o) {
        try {
            return Long.valueOf(o);
        } catch (final Exception e){
            return null;
        }
    }

}
