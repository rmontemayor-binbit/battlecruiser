/**
 *
 * Default package of battlecruiser you must not use this package, its only for
 * the main class.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser;
