package com.binbit.battlecruiser.exception.unchecked;

public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -5885123214070490325L;
    public static final String ALREADY_SUBSCRIBED_CODE = "01";

    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
