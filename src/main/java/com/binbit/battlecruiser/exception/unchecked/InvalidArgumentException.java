package com.binbit.battlecruiser.exception.unchecked;

public class InvalidArgumentException extends RuntimeException {

    private static final long serialVersionUID = 6601175621803667509L;

    public InvalidArgumentException() {
        super();
    }

    public InvalidArgumentException(final String message) {
        super(message);
    }

}
