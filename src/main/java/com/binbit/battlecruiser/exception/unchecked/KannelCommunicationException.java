package com.binbit.battlecruiser.exception.unchecked;

public class KannelCommunicationException extends RuntimeException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4110090479922587674L;

    public KannelCommunicationException() {
        super();
    }

    public KannelCommunicationException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public KannelCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public KannelCommunicationException(String message) {
        super(message);
    }

    public KannelCommunicationException(Throwable cause) {
        super(cause);
    }

}
