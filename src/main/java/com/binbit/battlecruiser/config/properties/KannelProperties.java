package com.binbit.battlecruiser.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * Bean representation of kannel properties in application.yml.
 *
 * @author rmontemayor
 *
 */
@Component
@Getter
@Setter
@ConfigurationProperties("kannel")
public class KannelProperties {
    /**
     * The kannel url to connect.
     */
    private String url;
    /**
     * The username for use kannel.
     */
    private String username;
    /**
     * The password for use kannel.
     */
    private String password;

}
