/**
 * Package for configuration classes.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser.config.properties;
