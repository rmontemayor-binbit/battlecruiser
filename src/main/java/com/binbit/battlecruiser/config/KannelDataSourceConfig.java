package com.binbit.battlecruiser.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(basePackages = "com.binbit.battlecruiser.dao.kannel" , sqlSessionFactoryRef = "kannelSqlSessionFactory")
public class KannelDataSourceConfig {

    @Bean("kannelHikariConfig")
    @ConfigurationProperties(prefix = "spring.kannelDs.hikari")
    public HikariConfig kannelHikariConfig() {
        return new HikariConfig();
    }

    @Bean("kannelDatasource")
    public DataSource dataSource() {
        return new HikariDataSource(kannelHikariConfig());
    }

    @Bean("kannelDataSourceTransactionManager")
    public DataSourceTransactionManager transactionManager() {
      return new DataSourceTransactionManager(dataSource());
    }

    @Bean("kannelSqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
      final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
      sessionFactory.setDataSource(dataSource());
      return sessionFactory.getObject();
    }
}
