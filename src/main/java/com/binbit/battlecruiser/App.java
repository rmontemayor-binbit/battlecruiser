package com.binbit.battlecruiser;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * This class starts the spring boot application.
 *
 *
 * @author rmontemayor
 *
 */
@EnableCircuitBreaker
@SpringBootApplication
@EnableCaching
@EnableRabbit
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class App extends SpringBootServletInitializer {

    /**
     * Main class that starts the spring boot application.
     *
     * @param args The args for running this.
     */
    public static void main(final String[] args) {
        SpringApplication.run(App.class, args);
    }

    /**
     * Injected bean added for hystrix supports.
     *
     * @param builder The RestTemplateBuilder.
     * @return RestTemplate The template builded.
     */
    @Bean
    public RestTemplate rest(final RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public Queue sendKannelMessageQueue() {
        return new Queue("Send_Kannel_Message");
    }

}
