package com.binbit.battlecruiser.service;

import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_CANCEL_PATH;
import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_EXTEND_PATH;
import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_REGISTER_PATH;
import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_STATUS_PATH;

import java.net.URI;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.binbit.battlecruiser.dto.ApiResponseDTO;
import com.binbit.battlecruiser.dto.SubscriptionCancelDTO;
import com.binbit.battlecruiser.dto.SubscriptionDTO;
import com.binbit.battlecruiser.dto.SubscriptionStatusDTO;
import com.binbit.battlecruiser.dto.SubscriptionStatusParamDTO;
import com.binbit.battlecruiser.dto.SubscriptionExtendDTO;
import com.binbit.battlecruiser.service.model.ConfigurationService;
import com.binbit.battlecruiser.util.Utils;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class SubscriptionServiceImpl implements SubscriptionService {

    /** SubscriptionServiceImpl's log. */
    private static final Logger LOG = LoggerFactory.getLogger(SubscriptionServiceImpl.class);

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public boolean isAlreadySubscribed(Long serviceId, String msisdn) {
        LOG.debug("isAlreadySubscribed() [{}] [{}]", serviceId, msisdn);
        final SubscriptionStatusParamDTO param = new SubscriptionStatusParamDTO();
        param.setServiceId(serviceId);
        param.setMsisdn(msisdn);
        return Status.ACTIVE.id().equals(getStatus(param).getStatusId());
    }

    @Override
    public SubscriptionStatusDTO getStatus(SubscriptionStatusParamDTO param) {
        try {
            final URIBuilder builder = new URIBuilder(
                    new URI(configurationService.getApiCoreUrl() + SUBSCRIPTION_STATUS_PATH));
            builder.addParameter("serviceId", Utils.toString(param.getServiceId()));
            builder.addParameter("msisdn", Utils.toString(param.getMsisdn()));
            final ApiResponseDTO response = restTemplate.getForObject(builder.build(), ApiResponseDTO.class);
            if ("success".equals(response.getStatus())) {
                final SubscriptionStatusDTO subscription = new SubscriptionStatusDTO();
                subscription.setStatusId(response.getData().getStatusId());
                subscription.setStatusMessage(response.getData().getStatusMessage());
                return subscription;
            }
        } catch (final Exception e) {
            LOG.error("-- subscribe() [{}]", param, e);
        }
        return new SubscriptionStatusDTO();
    }

    @Override
    public SubscriptionStatusDTO subscribe(SubscriptionDTO param) {
        LOG.debug("subscribe() [{}]", param);
        try {
            final URIBuilder builder = new URIBuilder(
                    new URI(configurationService.getApiCoreUrl() + SUBSCRIPTION_REGISTER_PATH));
            builder.addParameter("serviceId", Utils.toString(param.getServiceId()));
            builder.addParameter("msisdn", Utils.toString(param.getMsisdn()));
            builder.addParameter("shortCode", Utils.toString(param.getShortcode()));
            builder.addParameter("keyword", Utils.toString(param.getKeyword()));
            builder.addParameter("mccMnc", Utils.toString(param.getCarrierId())); //TODO revisar que venga bien
            builder.addParameter("mediaId", Utils.toString(param.getMediaId()));
            builder.addParameter("promoId", Utils.toString(param.getPromoId()));
            builder.addParameter("origin", Utils.toString(param.getChannel())); //TODO
            builder.addParameter("externalServiceId", Utils.toString(param.getExternalServiceId()));
            builder.addParameter("externalSubServiceId", Utils.toString(param.getExternalSubServiceId()));
            builder.addParameter("externalSubscriptionId", Utils.toString(param.getExternalSubscriptionId()));
            builder.addParameter("nextBillDate", Utils.toString(param.getNextBillDate()));
            builder.addParameter("tsRegister", Utils.toString(param.getNextBillDate())); //TODO

            final ApiResponseDTO response = restTemplate.getForObject(builder.build(), ApiResponseDTO.class);
            if ("success".equals(response.getStatus())) {
                final SubscriptionStatusDTO status = new SubscriptionStatusDTO();
                status.setStatusId(response.getData().getStatusId());
                status.setStatusMessage(response.getData().getStatusMessage());
                return status;
            }
        } catch (final Exception e) {
            LOG.error("-- subscribe() [{}]", param, e);
        }
        final SubscriptionStatusDTO status = new SubscriptionStatusDTO();
        status.setStatusId(Status.NOT_REGISTER.id());
        status.setStatusMessage(StringUtils.EMPTY);
        return status;
    }

    @Override
    public SubscriptionStatusDTO unsubscribe(SubscriptionCancelDTO param) {
        LOG.debug("unsubscribe() -- [{}]", param);
        try {
            final URIBuilder builder = new URIBuilder(
                    new URI(configurationService.getApiCoreUrl() + SUBSCRIPTION_CANCEL_PATH));
            builder.addParameter("serviceId", Utils.toString(param.getServiceId()));
            builder.addParameter("msisdn", param.getMsisdn());
            builder.addParameter("mccMnc", Utils.toString(param.getChannel())); //TODO
            builder.addParameter("origin", Utils.toString(param.getChannel())); //TODO
            builder.addParameter("tsRegister", Utils.toString(param.getChannel())); //TODO

            final ApiResponseDTO response = restTemplate.getForObject(builder.build(), ApiResponseDTO.class);
            if ("success".equals(response.getStatus())) {
                final SubscriptionStatusDTO status = new SubscriptionStatusDTO();
                status.setStatusId(response.getData().getStatusId());
                status.setStatusMessage(response.getData().getStatusMessage());
                return status;
            }
        } catch (final Exception e) {
            LOG.error("-- unsubscribe() [{}]", param, e);
        }
        final SubscriptionStatusDTO status = new SubscriptionStatusDTO();
        status.setStatusId(Status.UNKNOWN.id());
        status.setStatusMessage(StringUtils.EMPTY);
        return status;
    }

    @Override
    public void extend(SubscriptionExtendDTO param) {
        LOG.debug("-- extend() [{}]", param);
        try {
            restTemplate.getForObject(configurationService.getApiCoreUrl() + "", ApiResponseDTO.class);
            final URIBuilder builder = new URIBuilder(
                    new URI(configurationService.getApiCoreUrl() + SUBSCRIPTION_EXTEND_PATH));
            //TODO
            builder.addParameter("mccMnc", Utils.toString(param.getServiceId()));
            builder.addParameter("servicdId", param.getMsisdn());
            builder.addParameter("msisdn", Utils.toString(param.getNextBillDate()));
            builder.addParameter("nextBillDate", Utils.toString(param.getNextBillDate()));
            builder.addParameter("credits", Utils.toString(param.getCredits()));

            final ApiResponseDTO response = restTemplate.getForObject(builder.build(), ApiResponseDTO.class);
            if ("success".equals(response.getStatus())) {
                //TODO
            } else {

            }
        } catch (final Exception e) {
            LOG.error("-- extend() [{}]", param, e);
        }

    }
}
