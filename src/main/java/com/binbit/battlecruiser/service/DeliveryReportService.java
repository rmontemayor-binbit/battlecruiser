package com.binbit.battlecruiser.service;

public interface DeliveryReportService {
    void process(Long id, Integer type);
}
