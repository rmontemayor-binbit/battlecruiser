package com.binbit.battlecruiser.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.model.Attempt;
import com.binbit.battlecruiser.service.model.AttemptService;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class DeliveryReportServiceImpl implements DeliveryReportService {

    /** DeliveryReportServiceImpl's log. */
    private static final Logger LOG = LoggerFactory.getLogger(DeliveryReportServiceImpl.class);


    @Autowired
    private AttemptService attemptService;
    @Autowired
    private BillService billService;

    @Override
    public void process(Long id, Integer type) {
        switch (type) {
        case KannelService.DLR_TYPE_SUBMIT:
            final Attempt attempt = attemptService.get(id);
            LOG.debug("process() -- attemptId[{}]", attempt.getAttemptId());
            //subscriptionService.bill(attempt.getServiceId(), attempt.getMsisdn());
            //billService.notifySuccessfulBill();
            //TODO avisarle a promos
            // Fue de promos?
            // POST  http://promoswap.enterfactory.com/notify/km/attempt_report
            //Params:
            //    "msisdn" - MSISDN
            //    "carrier" - ID CARRIER
            //    "attempt" - ATTEMPT ID
            //    "bill":{"message":"subscriber without funds","code":200} - BILL message and code
            //    "subscription":{"message":"subscriber without funds","code":200} - SUBSCRIPTION - message and code
            //Answers:
            //Success Response : ["success" => true, "status" => 1, "message" => "Attempt updated correctly"]
            //        Failed Response: ["success" => false, "status" => 0, "message" => "Attempt {$this->input->attempt} not found"]
            break;

        case KannelService.DLR_TYPE_REJECTED:
            final Attempt rejectedAttempt = attemptService.get(id);
            LOG.debug("process() -- attemptId[{}]", rejectedAttempt.getAttemptId());
            //subscriptionService.rejectedBill();
            break;
        default:
            LOG.info("process() -- No process inmplementation for id[{}] type[{}]", id, type);
            break;
        }
    }


}
