package com.binbit.battlecruiser.service;

import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_REGISTER_MOBILE_ORIGINATED_PATH;

import java.net.URI;
import java.util.Date;

import org.apache.http.client.utils.URIBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.binbit.battlecruiser.dto.ApiResponseDTO;
import com.binbit.battlecruiser.dto.SubscriptionCancelDTO;
import com.binbit.battlecruiser.dto.SubscriptionDTO;
import com.binbit.battlecruiser.dto.kannel.KannelMessageDTO;
import com.binbit.battlecruiser.dto.kannel.SubscriptionRequestDTO;
import com.binbit.battlecruiser.exception.unchecked.BusinessException;
import com.binbit.battlecruiser.model.Attempt;
import com.binbit.battlecruiser.model.Carrier;
import com.binbit.battlecruiser.model.Keyword;
import com.binbit.battlecruiser.model.ServiceModel;
import com.binbit.battlecruiser.service.model.AttemptService;
import com.binbit.battlecruiser.service.model.AttemptService.AttemptType;
import com.binbit.battlecruiser.service.model.CarrierService;
import com.binbit.battlecruiser.service.model.ConfigurationService;
import com.binbit.battlecruiser.service.model.KeywordService;
//import com.binbit.battlecruiser.service.model.MessageService;
import com.binbit.battlecruiser.service.model.ServiceModelService;
import com.binbit.battlecruiser.util.Utils;

import lombok.Getter;
import lombok.Setter;

import static com.binbit.battlecruiser.util.Constants.SUCCESS;
@Service
@Getter
@Setter
public class SmsServiceImpl implements SmsService {

    /** SmsServiceImpl's log. */
    private static final Logger LOG = LoggerFactory.getLogger(SmsServiceImpl.class);

    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private KannelService kannelService;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private AttemptService attemptService;
    @Autowired
    private BillService billService;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ServiceModelService serviceModelService;
    @Autowired
    private KeywordService keywordService;
    @Autowired
    private CarrierService carrierService;
    //@Autowired
    //private MessageService messageService;

    @Override
    public void validateAndSubscribe(SubscriptionRequestDTO subscription) {
        validateSubscription(subscription);
        subscribe(subscription);
    }

    @Override
    public void validateSubscription(SubscriptionRequestDTO request) {
        final Long serviceId = configurationService.getServiceId(request.getShortCode(), request.getSmscId());
        if (subscriptionService.isAlreadySubscribed(serviceId, request.getMsisdn())) {
            final KannelMessageDTO message = new KannelMessageDTO();
            message.setFrom(request.getShortCode());
            message.setTo(request.getMsisdn());
            message.setText(configurationService.getAlreadySubscribedMessage(serviceId));
            kannelService.sendSms(message);
            throw new BusinessException("Already subscribed");
        }
    }


    @Override
    public void subscribe(SubscriptionRequestDTO request) {

        final Long serviceId = configurationService.getServiceId(request.getShortCode(), request.getSmscId());
        final Attempt attempt = saveAttempt(request, serviceId, AttemptType.SMS_SUBSCRIBE);


        subscriptionService.subscribe(getSubscriptionDTO(request));

        //TODO registerMo(request);

        final KannelMessageDTO message = new KannelMessageDTO();
        message.setFrom(configurationService.getFreeFrom(serviceId));
        message.setTo(request.getMsisdn());
        message.setText(configurationService.getWelcomeMessage(serviceId));
        kannelService.sendSms(message);

        billService.bill(serviceId, request.getMsisdn(), attempt.getId());

    }

    private SubscriptionDTO getSubscriptionDTO(SubscriptionRequestDTO request) {
        final SubscriptionDTO result = new SubscriptionDTO();
        BeanUtils.copyProperties(request, result);
        return result;

    }

    private Attempt saveAttempt(SubscriptionRequestDTO request, Long serviceId, AttemptType type) {
        final Attempt attempt = new Attempt();
        attempt.setAttemptId(null);
        attempt.setIpRemote(null);
        attempt.setMsisdn(request.getMsisdn());
        attempt.setPromoId(null);
        attempt.setPublisherId(null);
        attempt.setServiceId(serviceId);
        attempt.setAttemptTypeId(type.id());
        return attemptService.insert(attempt);
    }


    @Override
    public void unSubscribe(SubscriptionRequestDTO request) {
        final Long serviceId = configurationService.getServiceId(request.getShortCode(), request.getSmscId());
        saveAttempt(request, serviceId, AttemptType.SMS_UNSUBSCRIBE);
        //Validate
        if (subscriptionService.isAlreadySubscribed(serviceId, request.getMsisdn())) {
            final SubscriptionCancelDTO subscriptionCancelDTO = new SubscriptionCancelDTO();
            subscriptionCancelDTO.setServiceId(serviceId);
            subscriptionCancelDTO.setMsisdn(request.getMsisdn());
            subscriptionCancelDTO.setShortcode(request.getShortCode() == null ? null : Integer.valueOf(request.getShortCode()));
            //subscriptionCancelDTO.setKeyword(request.getKeyword()); TODO de donde sacar el keyword
            //subscriptionCancelDTO.setCarrierId(request.getcarrierId()); // TODO de donde sacar el kewyord
            //subscriptionCancelDTO.setChannel(request.getChannel()); // TODO de donde sacar el channel
            subscriptionService.unsubscribe(subscriptionCancelDTO);

            final KannelMessageDTO message = new KannelMessageDTO();
            message.setFrom(configurationService.getFreeFrom(serviceId));
            message.setTo(request.getMsisdn());
            message.setText(configurationService.getGoodByeMessage(serviceId));
            kannelService.sendSms(message);

        } else {
            final KannelMessageDTO message = new KannelMessageDTO();
            message.setFrom(request.getShortCode());
            message.setTo(request.getMsisdn());
            message.setText(configurationService.getNotSubscribedMessage(serviceId));
            kannelService.sendSms(message);
        }
    }

    @Override
    public void sendHelpMessage(SubscriptionRequestDTO request) {
        // TODO
        LOG.debug("sendHelpMessage() -- sending Help Message");
        //throw new NotImplementedException();
        /*final Carrier carrier = carrierService.getByIdSmsc(request.getIdSmsc());
        final ServiceModel service = serviceModelService.getService(request);

        final KannelMessageDTO message = new KannelMessageDTO();
        message.setFrom(messageService.getFreeFrom(carrier.getId(), service.getId()));
        message.setTo(request.getMsisdn());
        message.setText(messageService.getHelpMessage(carrier.getId(), service.getId()));
        kannelService.sendSms(message);*/

    }

    @Override
    public void process(SubscriptionRequestDTO request) {
        //Identificar
        LOG.debug("proccess() -- request[{}]", request);
        final Carrier carrier = carrierService.getBySmscId(request.getSmscId());
        if (carrier.getId() == null) {
            throw new RuntimeException(String.format("Carrier with smsc_id %s isn't in the database", request.getSmscId()));
        }
        LOG.debug("proccess() -- found carrier[{}]", carrier);

        final ServiceModel service = serviceModelService.find(carrier.getId(), request.getShortCode(), request.getText());
        LOG.debug("proccess() -- found service[{}]", service);

        final Keyword keyword = keywordService.find(carrier.getId(), service.getId(), request.getText());
        LOG.debug("proccess() -- found keyword[{}]", keyword);

        if (keyword.getId() == null) {
            sendHelpMessage(request);
        }

        // Procesar
        final Date timeStampOfArrive = new Date();
        if (KeywordService.Type.OPTIN.id().equals(keyword.getKeywordPolicyId())){
            //request.setKeyword(keyword.getValue());
            //validateAndSubscribe(request);
            LOG.debug("proccess() -- Se proceso un optin");
        } else  if (KeywordService.Type.OPTOUT.id().equals(keyword.getKeywordPolicyId())){
            unSubscribe(request);
        } else  if (KeywordService.Type.STOP_ALL.id().equals(keyword.getKeywordPolicyId())){
            //TODO
        } else  if (KeywordService.Type.HELP.id().equals(keyword.getKeywordPolicyId())){
            sendHelpMessage(request);
        } else  if (KeywordService.Type.LIST.id().equals(keyword.getKeywordPolicyId())){
            //TODO
        } else  if (KeywordService.Type.LIST_ALL.id().equals(keyword.getKeywordPolicyId())){
            //TODO
        }

        // Notificar
        registerMO(request, service.getServiceId(), timeStampOfArrive);

    }


    private void registerMO(SubscriptionRequestDTO request, Long serviceId, Date timeStampOfArrive) {
        try {
            final Carrier carrier = carrierService.getBySmscId(request.getSmscId());
            final URIBuilder builder = new URIBuilder(new URI(configurationService.getApiCoreUrl() + SUBSCRIPTION_REGISTER_MOBILE_ORIGINATED_PATH));
            // create request body
            final JSONObject body = new JSONObject();
            body.put("serviceId", serviceId);
            body.put("msisdn", request.getMsisdn());
            body.put("shortCode", Utils.toLong(request.getShortCode()));
            body.put("mccMnc", carrier.getId());
            body.put("smsc", request.getSmscId());
            body.put("text", request.getText());
            body.put("tsRegister", Utils.toString(timeStampOfArrive));
            LOG.debug("registerMO() -- body[{}]", body);
            // set headers
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            final HttpEntity<String> entity = new HttpEntity<String>(body.toString(), headers);

            final ResponseEntity<ApiResponseDTO> response = restTemplate.postForEntity(builder.build(), entity, ApiResponseDTO.class);
            LOG.debug("registerMO() -- response[{}]", response);
            if (!SUCCESS.equals(response.getBody().getStatus())) {
                LOG.error("-- registerMO() request[{}] response[{}]", request, response);
            }
        } catch (final Exception e) {
            LOG.error("-- registerMO() request[{}] serviceId[{}]", request, serviceId, e);
        }
    }

}
