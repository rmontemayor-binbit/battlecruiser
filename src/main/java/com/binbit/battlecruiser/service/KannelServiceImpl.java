package com.binbit.battlecruiser.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.binbit.battlecruiser.config.properties.KannelProperties;
import com.binbit.battlecruiser.dto.kannel.KannelMessageDTO;
import com.binbit.battlecruiser.exception.unchecked.InvalidArgumentException;
import com.binbit.battlecruiser.exception.unchecked.KannelCommunicationException;
import com.binbit.battlecruiser.model.kannel.SendSmsWithBLOBs;
import com.binbit.battlecruiser.service.model.ConfigurationService;
import com.binbit.battlecruiser.service.model.kannel.SendSmsService;
import com.binbit.battlecruiser.util.Utils;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Kannel service implementation, uses http request for Kannel interaction.
 *
 * @author rmontemayor
 *
 */
@Service
@Getter
@Setter
public class KannelServiceImpl implements KannelService {

    /** Log de KannelServiceImpl. */
    private static final Logger LOG = LoggerFactory.getLogger(KannelServiceImpl.class);

    /**
     * KannelProperties, inyected by spring from application.yml.
     */
    @Autowired
    private KannelProperties kannelProperties;

    @Autowired
    private SendSmsService sendSmsService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Send SMS Message with Kannel.
     *
     * @param message The message to send.
     * @return True if kannel accepts the message.
     */
    @Override
    @HystrixCommand(fallbackMethod = "fallbackSendSms", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000" )
    } )
    public boolean sendSms(final KannelMessageDTO message) {
        validate(message);
        try {
            final URIBuilder builder = new URIBuilder(kannelProperties.getUrl());
            builder.addParameter("username", kannelProperties.getUsername());
            builder.addParameter("password", kannelProperties.getPassword());
            builder.addParameter("from", message.getFrom());
            builder.addParameter("to", message.getTo());
            builder.addParameter("text", message.getText());
            builder.addParameter("dlr-mask", Utils.toString(message.getDeliveryReportMask()));
            builder.addParameter("dlr-url", message.getDeliveryReportUrl());

            final ResponseEntity<String> response = restTemplate.getForEntity(builder.build(), String.class);
            LOG.debug("send() -- response[{}] message[{}]", response.getBody(), message);
            return StringUtils.contains(response.getBody(), "Accepted")
                    || StringUtils.contains(response.getBody(), "Queued");
        } catch (final Exception e) {
            LOG.error("send() -- message[{}]", message, e);
            throw new KannelCommunicationException(e);
        }
    }

    public boolean fallbackSendSms(final KannelMessageDTO message) {
        LOG.info("-- fallbackSendSms() message[{}]", message);
        final SendSmsWithBLOBs sms = new SendSmsWithBLOBs();
        sms.setSender(message.getFrom());
        sms.setReceiver(message.getTo());
        sms.setMsgdata(message.getText());
        sms.setDlrMask(message.getDeliveryReportMask() == null ? null : message.getDeliveryReportMask().longValue());
        sms.setDlrUrl(message.getDeliveryReportUrl());
        sendSmsService.insert(sms);
        //TODO registerMT(message);
        return false;
    }

    //TODO
    /*private void registerMT(KannelMessageDTO message) {
        try {
            final URIBuilder builder = new URIBuilder(new URI(configurationService.getApiCoreUrl() + SUBSCRIPTION_REGISTER_MOBILE_TERMINATED_PATH));
            builder.addParameter("serviceId", Utils.toString(serviceId));
            builder.addParameter("msisdn", request.getMsisdn());
            builder.addParameter("shortCode", request.getShortCode());
            builder.addParameter("carrierId", "uy"); //TODO de donde sacar el carrier
            builder.addParameter("smsc", request.getSmsc());
            builder.addParameter("text", request.getText());
            builder.addParameter("registerDate", StringUtils.EMPTY); // TODO validar que para el servicio se igual si lo registro asi
            final ResponseEntity<ApiResponseDTO> response = restTemplate.getForEntity(builder.build(), ApiResponseDTO.class);
            if (!SUCCESS.equals(response.getBody().getStatus())) {
                LOG.error("-- registerMO() request[{}] response[{}]", request, response);
            }
        } catch (final Exception e) {
            LOG.error("-- registerMO() request[{}] serviceId[{}]", request, serviceId, e);
        }

    }*/

    public void validate(final KannelMessageDTO message) {
        if (message == null) {
            throw new InvalidArgumentException("The message is required");
        }
        if (StringUtils.isEmpty(message.getFrom())) {
            throw new InvalidArgumentException("From is required");
        }
        if (StringUtils.isEmpty(message.getTo())) {
            throw new InvalidArgumentException("To is required");
        }

        if (StringUtils.isEmpty(message.getText())) {
            throw new InvalidArgumentException("Text is required");
        }
    }
}
