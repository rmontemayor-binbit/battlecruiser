/**
 *
 * Base package for service interfaces.
 *
 * @author rmontemayor
 *
 */
package com.binbit.battlecruiser.service;
