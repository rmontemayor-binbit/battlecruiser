package com.binbit.battlecruiser.service;

import com.binbit.battlecruiser.dto.promos.WapSubscribeRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapConfirmPinRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapResponseDTO;
import com.binbit.battlecruiser.dto.promos.WapSendPinRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapUnsubscribeRequestDTO;

public interface WapService {

    static final int PIN_LENGTH = 4;

    WapResponseDTO saveAttemptValidateSubscribeAndBill(WapSubscribeRequestDTO request);

    WapResponseDTO unsubscribe(WapUnsubscribeRequestDTO request);

    WapResponseDTO sendPin(WapSendPinRequestDTO request);

    WapResponseDTO confirmPinAndSubscribe(WapConfirmPinRequestDTO request);

}
