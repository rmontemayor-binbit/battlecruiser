package com.binbit.battlecruiser.service;

import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_REGISTER_BILL_PATH;
import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_REGISTER_FAILED_BILL_PATH;
import static com.binbit.battlecruiser.util.Constants.SUBSCRIPTION_CHANGE_NEXT_BILL_DATE_PATH;

import java.net.URI;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.binbit.battlecruiser.dto.ApiResponseDTO;
import com.binbit.battlecruiser.dto.ChangeNextBillDateDTO;
import com.binbit.battlecruiser.dto.RegisterBillDTO;
import com.binbit.battlecruiser.dto.kannel.KannelMessageDTO;
import com.binbit.battlecruiser.service.model.ConfigurationService;
import com.binbit.battlecruiser.util.Utils;
import com.fasterxml.jackson.databind.util.ISO8601Utils;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class BillServiceImpl implements BillService {

    /** BillServiceImpl's log. */
    private static final Logger LOG = LoggerFactory.getLogger(BillServiceImpl.class);


    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private KannelService kannelService;

    @Autowired
    private RestTemplate restTemplate;


    @Override
    public BillStatus bill(Long serviceId, String msisdn, Long attemptId) {
        try {
            // send bill message
            final KannelMessageDTO billMessge = new KannelMessageDTO();
            billMessge.setFrom(configurationService.getBillFrom(serviceId));
            billMessge.setTo(msisdn);
            billMessge.setText(configurationService.getFirstBillMessage(serviceId));
            billMessge.setDeliveryReportMask(KannelService.DLR_MASK_ALL);
            billMessge.setDeliveryReportUrl(configurationService.getKannelDlrUrl(serviceId) + "?id=" + attemptId + "&type=%d");
            kannelService.sendSms(billMessge);
            return BillStatus.QUEUED;
            //TODO preguntar si desde aqui se avisa a api-core
        } catch (final Exception e){
            LOG.error("-- bill()", e);
        }
        // TODO Esto es mentira si ocurre una excepcion hay que encolar la peticion en el rabbit
        return BillStatus.QUEUED;
    }

    public void changeNextBillDate(ChangeNextBillDateDTO param) {
        try {
            LOG.debug("-- changeNextBillDate() [{}]", param);
            final URIBuilder builder = new URIBuilder(new URI(configurationService.getApiCoreUrl() + SUBSCRIPTION_CHANGE_NEXT_BILL_DATE_PATH));
            builder.addParameter("serviceId", Utils.toString(param.getServiceId()));
            builder.addParameter("msisdn", param.getMsisdn());
            builder.addParameter("nextBillDate", ISO8601Utils.format(param.getNextBillDate()));
            final ApiResponseDTO response = restTemplate.getForObject(builder.build(), ApiResponseDTO.class);
            if ("success".equals(response.getStatus())) {
                //TODO
            } else {

            }
        } catch (final Exception e) {
            LOG.error("-- changeNextBillDate() ", e);
        }
    }

    @Override
    public void registerBill(RegisterBillDTO param, boolean successful) {
        try {
            LOG.debug("-- changeNextBillDate() [{}]", param);
            final String endpoint = successful ? SUBSCRIPTION_REGISTER_BILL_PATH : SUBSCRIPTION_REGISTER_FAILED_BILL_PATH;
            final URIBuilder builder = new URIBuilder(new URI(configurationService.getApiCoreUrl() + endpoint));
            builder.addParameter("serviceId", Utils.toString(param.getServiceId()));
            builder.addParameter("msisdn", param.getMsisdn());
            builder.addParameter("amount", Utils.toString(param.getAmount()));
            builder.addParameter("billingCode", param.getBillingCode());
            builder.addParameter("shortCode", Utils.toString(param.getShortCode()));
            builder.addParameter("carrierId", Utils.toString(param.getCarrierId()));
            builder.addParameter("channel", param.getChannel());
            builder.addParameter("mediaId", Utils.toString(param.getMediaId()));
            builder.addParameter("publisherId", Utils.toString(param.getPublisherId()));
            builder.addParameter("networkId", Utils.toString(param.getNetworkId()));
            builder.addParameter("promoId", Utils.toString(param.getPromoId()));

            builder.addParameter("errorCode", Utils.toString(param.getErrorCode()));
            builder.addParameter("errorMessage", Utils.toString(param.getErrorMessage()));
            builder.addParameter("carrierErrorCode", Utils.toString(param.getCarrierErrorCode()));
            builder.addParameter("carrierErrorMessage", Utils.toString(param.getCarrierErrorMessage()));

            final ApiResponseDTO response = restTemplate.getForObject(builder.build(), ApiResponseDTO.class);
            if ("success".equals(response.getStatus())) {
                //TODO
            } else {

            }
        } catch (final Exception e) {
            LOG.error("-- changeNextBillDate() ", e);
        }
    }

}
