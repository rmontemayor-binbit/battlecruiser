package com.binbit.battlecruiser.service;

import com.binbit.battlecruiser.dto.kannel.KannelMessageDTO;
import com.binbit.battlecruiser.dto.kannel.SmsResponseDTO;
import com.binbit.battlecruiser.dto.kannel.StatusDTO;

/**
 * Kannel service inteface, all services for kannel interaction goes here.
 *
 * @author rmontemayor
 *
 */
public interface KannelService {
    /**
     * Its the default success respond to kannel.
     */
    public static final SmsResponseDTO SUCCESS_KANNEL_RESPONSE = new SmsResponseDTO(new StatusDTO("200", "success"));

    public static final int DLR_TYPE_SUCCESS = 1;
    public static final int DLR_TYPE_FAILURE = 2;
    public static final int DLR_TYPE_BUFFERED = 4;
    public static final int DLR_TYPE_SUBMIT = 8;
    public static final int DLR_TYPE_REJECTED = 16;
    public static final int DLR_MASK_ALL = DLR_TYPE_SUCCESS + DLR_TYPE_FAILURE + DLR_TYPE_BUFFERED + DLR_TYPE_SUBMIT + DLR_TYPE_REJECTED;


    /**
     * Send SMS Message with Kannel.
     *
     * @param message The message to send.
     * @return True if kannel accepts the message.
     */
    public boolean sendSms(KannelMessageDTO message);
}
