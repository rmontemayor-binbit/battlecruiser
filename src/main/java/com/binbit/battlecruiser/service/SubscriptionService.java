package com.binbit.battlecruiser.service;

import com.binbit.battlecruiser.dto.SubscriptionCancelDTO;
import com.binbit.battlecruiser.dto.SubscriptionDTO;
import com.binbit.battlecruiser.dto.SubscriptionStatusDTO;
import com.binbit.battlecruiser.dto.SubscriptionStatusParamDTO;
import com.binbit.battlecruiser.dto.SubscriptionExtendDTO;

public interface SubscriptionService {
    static final String CODE_SUCCESS = "100";
    static final String MESSAGE_SUCCESS = "success";

    static final String CODE_IN_PROCESS = "101";
    static final String MESSAGE_IN_PROCESS = "in_process";

    static final String CODE_USER_ALREADY_ACTIVE = "1312";
    static final String MESSAGE_USER_ALREADY_ACTIVE = "subscriber was active";

    static final String CODE_BLACKLISTED_USER = "800";
    static final String MESSAGE_BLACKLISTED_USER = "blacklisted user";

    static final String CODE_GENERIC_PARAMETER_ERROR = "900";
    static final String MESSAGE_GENERIC_PARAMETER_ERROR = "generic parameter error";

    // TODO validar cuales son los codigos para un usuario que no esta subscrito
    static final String CODE_USER_NOT_ACTIVE = "900";
    static final String MESSAGE_USER_NOT_ACTIVE = "user isn't active";

    // TODO validar cuales son los codigos para un usuario desusbscrito
    static final String CODE_USER_UNSUBSCRIBED = "900";
    static final String MESSAGE_USER_UNSUBSCRIBED = "unsubscribe user";

    static final String CODE_INVALID_PIN = "900";
    static final String MESSAGE_INVALID_PIN = "invalid pin code";

    static final String CODE_REJECTED = "900";
    static final String MESSAGE_REJECTED = "rejected";

    public enum Status {
        ACTIVE(1L), NOT_REGISTER(20L), BLACKLISTED(30L), UNKNOWN(0L);
        private final Long id;

        Status(Long id) {
            this.id = id;
        }
        public Long id(){
            return this.id;
        }
    }


    boolean isAlreadySubscribed(Long serviceId, String msisdn);
    void extend(SubscriptionExtendDTO param);
    SubscriptionStatusDTO unsubscribe(SubscriptionCancelDTO param);
    SubscriptionStatusDTO subscribe(SubscriptionDTO param);
    SubscriptionStatusDTO getStatus(SubscriptionStatusParamDTO param);
}
