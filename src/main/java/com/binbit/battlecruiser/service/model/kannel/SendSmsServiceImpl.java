package com.binbit.battlecruiser.service.model.kannel;

import java.util.List;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.kannel.SendSmsMapper;
import com.binbit.battlecruiser.model.kannel.SendSmsWithBLOBs;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class SendSmsServiceImpl implements SendSmsService {

    /** SendSmsServiceImpl's log. */
    private static final Logger LOG = LoggerFactory.getLogger(SendSmsServiceImpl.class);

    @Autowired
    private SendSmsMapper mapper;

    @Autowired
    private SqlSessionFactory kannelSqlSessionFactory;

    @Override
    public void bulkInsert(List<SendSmsWithBLOBs> records) {
        final boolean autoCommit = false;
        final SqlSession sqlSession = kannelSqlSessionFactory.openSession(ExecutorType.BATCH, autoCommit);
        try {
            final SendSmsMapper mapper = sqlSession.getMapper(SendSmsMapper.class);
            for (final SendSmsWithBLOBs record : records) {
                mapper.insert(record);
            }
            sqlSession.commit();
        } catch (final Exception e) {
            LOG.error("-- bulkInsert()", e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public SendSmsWithBLOBs insert(SendSmsWithBLOBs model) {
        mapper.insert(model);
        return model;

    }

}
