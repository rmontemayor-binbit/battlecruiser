package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.mapper.ConfigurationMapper;
import com.binbit.battlecruiser.model.Configuration;
import com.binbit.battlecruiser.model.ConfigurationExample;
import com.google.common.base.MoreObjects;
import com.google.common.util.concurrent.RateLimiter;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class ConfigurationServiceImpl implements ConfigurationService {

    @Autowired
    private ConfigurationMapper mapper;

    @Override
    public List<Configuration> getAll() {
        final ConfigurationExample example = new ConfigurationExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public Configuration get(Long id) {
        final Configuration result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new Configuration();
        }
        return result;
    }

    @Override
    public Configuration get(String code) {
        final ConfigurationExample example = new ConfigurationExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE)
        .andCodeEqualTo(code);
        final List<Configuration> lst = mapper.selectByExample(example);
        if (CollectionUtils.isEmpty(lst)) {
            return new Configuration();
        }
        return lst.get(0);
    }

    @Override
    public Configuration insert(Configuration model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public Configuration delete(Long id) {
        final Configuration result = new Configuration();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public Configuration update(Configuration model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }

    @Override
    @Cacheable("getWelcomeMessage")
    public String getWelcomeMessage(Long serviceId) {
        final String code = "service[" + serviceId + "].welcome";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getFirstBillMessage")
    public String getFirstBillMessage(Long serviceId) {
        final String code = "service[" + serviceId + "].bill";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getFreeFrom")
    public String getFreeFrom(Long serviceId) {
        final String code = "service[" + serviceId + "].freeFrom";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getBillFrom")
    public String getBillFrom(Long serviceId) {
        final String code = "service[" + serviceId + "].billFrom";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getKannelDlrUrl")
    public String getKannelDlrUrl(Long serviceId) {
        final String code = "service[" + serviceId + "].kannelDlrUrl";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getGoodByeMessage")
    public String getGoodByeMessage(Long serviceId) {
        final String code = "service[" + serviceId + "].goodBye";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("generatePin")
    public boolean generatePin(Long serviceId) {
        final String code = "service[" + serviceId + "].generatePin";
        return BooleanUtils.toBoolean(get(code).getValue());
    }

    @Override
    @Cacheable("getPinMessage")
    public String getPinMessage(Long serviceId) {
        final String code = "service[" + serviceId + "].pinMessage";
        return MoreObjects.firstNonNull(get(code).getValue(), "%s");
    }

    @Override
    @Cacheable("getServiceId")
    public Long getServiceId(String shortCode, String smsc) {
        final String code = "smsc[" + smsc +"].shortCode[" + shortCode + "].serviceId";
        return Long.valueOf(MoreObjects.firstNonNull(get(code).getValue(), "0"));
    }

    @Override
    @Cacheable("getAlreadySubscribedMessage")
    public String getAlreadySubscribedMessage(Long serviceId) {
        final String code = "service[" + serviceId + "].alreadySubscribedMessage";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getApiCoreUrl")
    public String getApiCoreUrl() {
        final String code = "apiCoreUrl";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getSubscribeCodes")
    public String[] getSubscribeCodes(Long serviceId) {
        final String code = "service[" + serviceId + "].subscribeCodes";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY).split(",");
    }

    @Override
    @Cacheable("getUnsubscribeCodes")
    public String[] getUnsubscribeCodes(Long serviceId) {
        final String code = "service[" + serviceId + "].unsubscribeCodes";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY).split(",");
    }

    @Override
    @Cacheable("getNotSubscribedMessage")
    public String getNotSubscribedMessage(Long serviceId) {
        final String code = "service[" + serviceId + "].notSubscribedMessage";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getHelpMessage")
    public String getHelpMessage(Long serviceId) {
        final String code = "service[" + serviceId + "].helpMessage";
        return MoreObjects.firstNonNull(get(code).getValue(), StringUtils.EMPTY);
    }

    @Override
    @Cacheable("getAllowedTransactionsPerSecond")
    public Double getAllowedTransactionsPerSecond(Long carrierId) {
        final String code = "carrierId[" + carrierId + "].allowedTransactionsPerSecond";
        return Double.valueOf(MoreObjects.firstNonNull(get(code).getValue(), "10")); //todo
    }

    @Override
    @Cacheable("getRateLimiter")
    public RateLimiter getRateLimiter(Long carrierId) {
        return RateLimiter.create(getAllowedTransactionsPerSecond(carrierId));
    }
}
