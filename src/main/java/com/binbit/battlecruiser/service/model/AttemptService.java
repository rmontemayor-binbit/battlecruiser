package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.Attempt;

public interface AttemptService {

    public enum AttemptType {
        SMS_SUBSCRIBE(10L), WAP_SUBSCRIBE(20L), SMS_UNSUBSCRIBE(30L), WAP_UNSUBSCRIBE(40L), SEND_PIN(50L),
            CONFIRM_PIN(60L);
        private final Long id;

        AttemptType(Long id) {
            this.id = id;
        }
        public Long id(){
            return this.id;
        }
    }

    List<Attempt> getAll();
    Attempt get(Long id);
    Attempt insert(Attempt model);
    Attempt delete(Long id);
    Attempt update(Attempt model);
    Attempt getByAttemptId(Long attemptId, AttemptType type);
}
