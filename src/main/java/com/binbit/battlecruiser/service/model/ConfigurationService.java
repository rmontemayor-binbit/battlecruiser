package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.Configuration;
import com.google.common.util.concurrent.RateLimiter;

public interface ConfigurationService {

    List<Configuration> getAll();
    Configuration get(Long id);
    Configuration get(String code);
    Configuration insert(Configuration model);
    Configuration delete(Long id);
    Configuration update(Configuration model);

    String getWelcomeMessage(Long serviceId);
    String getFirstBillMessage(Long serviceId);
    String getFreeFrom(Long serviceId);
    String getBillFrom(Long serviceId);
    String getKannelDlrUrl(Long serviceId);
    String getGoodByeMessage(Long serviceId);
    boolean generatePin(Long serviceId);
    String getPinMessage(Long serviceId);
    Long getServiceId(String shortCode, String smsc);
    String getAlreadySubscribedMessage(Long serviceId);
    String getApiCoreUrl();
    String[] getSubscribeCodes(Long serviceId);
    String[] getUnsubscribeCodes(Long serviceId);
    String getNotSubscribedMessage(Long serviceId);
    String getHelpMessage(Long serviceId);
    Double getAllowedTransactionsPerSecond(Long carrierId);
    RateLimiter getRateLimiter(Long carrierId);

}
