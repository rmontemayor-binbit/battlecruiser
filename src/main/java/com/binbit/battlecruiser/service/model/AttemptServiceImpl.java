package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.mapper.AttemptMapper;
import com.binbit.battlecruiser.model.Attempt;
import com.binbit.battlecruiser.model.AttemptExample;
import com.google.common.base.MoreObjects;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class AttemptServiceImpl implements AttemptService {

    @Autowired
    private AttemptMapper mapper;

    @Override
    public List<Attempt> getAll() {
        final AttemptExample example = new AttemptExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public Attempt get(Long id) {
        final Attempt result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new Attempt();
        }
        return result;
    }

    @Override
    public Attempt getByAttemptId(Long attemptId, AttemptType type) {
        final AttemptExample example = new AttemptExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE)
        .andAttemptIdEqualTo(attemptId)
        .andAttemptTypeIdEqualTo(type.id());
        example.setOrderByClause("id desc");
        final List<Attempt> list = mapper.selectByExample(example);
        return MoreObjects.firstNonNull(list.get(0), new Attempt());
    }

    @Override
    public Attempt insert(Attempt model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public Attempt delete(Long id) {
        final Attempt result = new Attempt();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public Attempt update(Attempt model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }
}
