package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.ServiceType;

public interface ServiceTypeService {

    List<ServiceType> getAll();
    ServiceType get(Long id);
    ServiceType insert(ServiceType model);
    ServiceType delete(Long id);
    ServiceType update(ServiceType model);

}
