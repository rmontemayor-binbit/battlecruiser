package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.mapper.CountryMapper;
import com.binbit.battlecruiser.model.Country;
import com.binbit.battlecruiser.model.CountryExample;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryMapper mapper;

    @Override
    public List<Country> getAll() {
        final CountryExample example = new CountryExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public Country get(Long id) {
        final Country result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new Country();
        }
        return result;
    }


    @Override
    public Country insert(Country model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public Country delete(Long id) {
        final Country result = new Country();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public Country update(Country model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }

}
