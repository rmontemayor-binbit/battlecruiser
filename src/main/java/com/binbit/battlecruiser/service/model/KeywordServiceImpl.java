package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.mapper.KeywordMapper;
import com.binbit.battlecruiser.model.Keyword;
import com.binbit.battlecruiser.model.KeywordExample;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class KeywordServiceImpl implements KeywordService {

    @Autowired
    private KeywordMapper mapper;

    @Autowired
    private CarrierService carrierService;

    @Override
    public List<Keyword> getAll() {
        final KeywordExample example = new KeywordExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public Keyword get(Long id) {
        final Keyword result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new Keyword();
        }
        return result;
    }


    @Override
    public Keyword insert(Keyword model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public Keyword delete(Long id) {
        final Keyword result = new Keyword();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public Keyword update(Keyword model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }

    @Override
    public List<Keyword> getKeywords(Long carrierId, Long serviceId) {
        if (serviceId == null) {
            return getGlobalKeywords(carrierId);
        }
       return mapper.selectByCarrierIdAndServiceId(carrierId, serviceId);
    }

    @Override
    public List<Keyword> getGlobalKeywords(Long carrierId) {
        return mapper.selectByCarrierId(carrierId);
    }

    @Override
    public Keyword find(Long carrierId, Long serviceId, String text) {
        final List<Keyword> keywords = getKeywords(carrierId, serviceId);
        for (final Keyword keyword : keywords){
            if (KeywordService.Policy.EXACT_MATCH.id().equals(keyword.getKeywordTypeId())) {
                if(keyword.getValue().equals(text)) {
                    return keyword;
                }
            } else if ( KeywordService.Policy.CONTAINS.id().equals(keyword.getKeywordTypeId())) {
                if(StringUtils.containsIgnoreCase(keyword.getName(), text)) {
                   return keyword;
                }
            }
        }
        return new Keyword();
    }

}
