package com.binbit.battlecruiser.service.model;

public interface MessageService {

    String getFreeFrom(Long carrierId, Long serviceId);

    String getHelpMessage(Long carrierId, Long serviceId);

}
