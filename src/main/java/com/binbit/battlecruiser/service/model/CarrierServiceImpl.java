package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.mapper.CarrierMapper;
import com.binbit.battlecruiser.model.Carrier;
import com.binbit.battlecruiser.model.CarrierExample;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class CarrierServiceImpl implements CarrierService {
    @Autowired
    private CarrierMapper mapper;

    @Override
    public List<Carrier> getAll() {
        final CarrierExample example = new CarrierExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public Carrier get(Long id) {
        final Carrier result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new Carrier();
        }
        return result;
    }


    @Override
    public Carrier insert(Carrier model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public Carrier delete(Long id) {
        final Carrier result = new Carrier();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public Carrier update(Carrier model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }

    @Override
    @Cacheable("getByIdSmsc")
    public Carrier getBySmscId(String smscId) {
        final CarrierExample example = new CarrierExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE)
        .andSmscIdEqualTo(smscId);
        final List<Carrier> resultList = mapper.selectByExample(example);
        if(CollectionUtils.isEmpty(resultList)) {
            return new Carrier();
        } else {
            return resultList.get(0);
        }
    }
}
