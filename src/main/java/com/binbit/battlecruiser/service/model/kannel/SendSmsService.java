package com.binbit.battlecruiser.service.model.kannel;

import java.util.List;

import com.binbit.battlecruiser.model.kannel.SendSmsWithBLOBs;

public interface SendSmsService {

    void bulkInsert(List<SendSmsWithBLOBs> records);

    SendSmsWithBLOBs insert(SendSmsWithBLOBs sms);
}
