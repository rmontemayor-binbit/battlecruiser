package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.Carrier;

public interface CarrierService {

    List<Carrier> getAll();
    Carrier get(Long id);
    Carrier insert(Carrier model);
    Carrier delete(Long id);
    Carrier update(Carrier model);



    Carrier getBySmscId(String smscId);

}
