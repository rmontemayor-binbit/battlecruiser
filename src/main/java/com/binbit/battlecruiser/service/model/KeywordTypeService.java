package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.KeywordType;

public interface KeywordTypeService {

    List<KeywordType> getAll();
    KeywordType get(Long id);
    KeywordType insert(KeywordType model);
    KeywordType delete(Long id);
    KeywordType update(KeywordType model);

}
