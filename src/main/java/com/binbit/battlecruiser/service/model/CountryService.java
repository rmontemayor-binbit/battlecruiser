package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.Country;

public interface CountryService {

    List<Country> getAll();
    Country get(Long id);
    Country insert(Country model);
    Country delete(Long id);
    Country update(Country model);

}
