package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.ServiceModel;

public interface ServiceModelService {

    List<ServiceModel> getAll();
    ServiceModel get(Long id);
    ServiceModel insert(ServiceModel model);
    ServiceModel delete(Long id);
    ServiceModel update(ServiceModel model);



    ServiceModel find(Long carrierId, String shortCode, String text);
}
