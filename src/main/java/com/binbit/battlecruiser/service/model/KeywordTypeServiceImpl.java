package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.mapper.KeywordTypeMapper;
import com.binbit.battlecruiser.model.KeywordType;
import com.binbit.battlecruiser.model.KeywordTypeExample;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class KeywordTypeServiceImpl implements KeywordTypeService {

    @Autowired
    private KeywordTypeMapper mapper;

    @Override
    public List<KeywordType> getAll() {
        final KeywordTypeExample example = new KeywordTypeExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public KeywordType get(Long id) {
        final KeywordType result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new KeywordType();
        }
        return result;
    }


    @Override
    public KeywordType insert(KeywordType model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public KeywordType delete(Long id) {
        final KeywordType result = new KeywordType();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public KeywordType update(KeywordType model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }

}
