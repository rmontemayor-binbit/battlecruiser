package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.binbit.battlecruiser.dao.mapper.ServiceTypeMapper;
import com.binbit.battlecruiser.model.ServiceType;
import com.binbit.battlecruiser.model.ServiceTypeExample;

public class ServiceTypeServiceImpl implements ServiceTypeService {
    @Autowired
    private ServiceTypeMapper mapper;

    @Override
    public List<ServiceType> getAll() {
        final ServiceTypeExample example = new ServiceTypeExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public ServiceType get(Long id) {
        final ServiceType result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new ServiceType();
        }
        return result;
    }


    @Override
    public ServiceType insert(ServiceType model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public ServiceType delete(Long id) {
        final ServiceType result = new ServiceType();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public ServiceType update(ServiceType model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }

}
