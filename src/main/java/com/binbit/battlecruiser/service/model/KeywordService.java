package com.binbit.battlecruiser.service.model;

import java.util.List;

import com.binbit.battlecruiser.model.Keyword;

public interface KeywordService {

    enum Type {
        OPTIN(10L), OPTOUT(20L), STOP_ALL(30L),
        HELP(40L), LIST(50L), LIST_ALL(60L);
        private final Long id;

        Type(Long id) {
            this.id = id;
        }
        public Long id(){
            return this.id;
        }
    }

    enum Policy {
        EXACT_MATCH(10L), CONTAINS(20L);
        private final Long id;

        Policy(Long id) {
            this.id = id;
        }
        public Long id(){
            return this.id;
        }
    }

    List<Keyword> getAll();
    Keyword get(Long id);
    Keyword insert(Keyword model);
    Keyword delete(Long id);
    Keyword update(Keyword model);


    List<Keyword> getKeywords(Long carrierId, Long serviceId);
    //List<Keyword> getByServiceId(Long serviceId);
    List<Keyword> getGlobalKeywords(Long carrierId);

    Keyword find(Long carrierId, Long serviceId, String text);

}
