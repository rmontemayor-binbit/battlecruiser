package com.binbit.battlecruiser.service.model;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dao.mapper.ServiceModelMapper;
import com.binbit.battlecruiser.model.Keyword;
import com.binbit.battlecruiser.model.ServiceModel;
import com.binbit.battlecruiser.model.ServiceModelExample;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class ServiceModelServiceImpl implements ServiceModelService {

    @Autowired
    private ServiceModelMapper mapper;


    @Autowired
    private CarrierService carrierService;

    @Autowired
    private KeywordService keywordService;



    @Override
    public List<ServiceModel> getAll() {
        final ServiceModelExample example = new ServiceModelExample();
        example.createCriteria().andActiveEqualTo(Boolean.TRUE);
        example.setOrderByClause("id");
        return mapper.selectByExample(example);
    }

    @Override
    public ServiceModel get(Long id) {
        final ServiceModel result = mapper.selectByPrimaryKey(id);
        if (result == null || !result.getActive()) {
            return new ServiceModel();
        }
        return result;
    }


    @Override
    public ServiceModel insert(ServiceModel model) {
        model.setCreatedBy("SYSTEM");
        model.setCreatedDate(new Date());
        model.setUpdatedBy("SYSTEM");
        model.setUpdatedDate(new Date());
        model.setActive(true);
        mapper.insert(model);
        return model;
    }

    @Override
    public ServiceModel delete(Long id) {
        final ServiceModel result = new ServiceModel();
        result.setId(id);
        result.setActive(Boolean.FALSE);
        result.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(result);
        return result;
    }

    @Override
    public ServiceModel update(ServiceModel model) {
        model.setUpdatedDate(new Date());
        mapper.updateByPrimaryKeySelective(model);
        return get(model.getId());
    }

    @Override
    public ServiceModel find(Long carrierId, String shortCode, String text) {
        final List<ServiceModel> services = mapper.selectByCarrierIdAndShortCode(carrierId, shortCode);
        for(final ServiceModel service : services) {
            final Keyword keyword = keywordService.find(carrierId, service.getId(), text);
            if (keyword.getId() != null){
                return service;
            }
        }
        return new ServiceModel();
    }


}
