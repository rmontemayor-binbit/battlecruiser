package com.binbit.battlecruiser.service;

import com.binbit.battlecruiser.dto.RegisterBillDTO;

public interface BillService {

    public enum BillStatus {
        QUEUED, BILLED, REJECTED
    }

    BillStatus bill(Long serviceId, String msisdn, Long attemptId);

    void registerBill(RegisterBillDTO param, boolean successful);

}
