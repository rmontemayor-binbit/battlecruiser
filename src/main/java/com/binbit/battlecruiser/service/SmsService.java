package com.binbit.battlecruiser.service;

import com.binbit.battlecruiser.dto.kannel.SubscriptionRequestDTO;

public interface SmsService {
    public void validateAndSubscribe(SubscriptionRequestDTO subscription);

    public void validateSubscription(SubscriptionRequestDTO subscription);

    public void subscribe(SubscriptionRequestDTO subscription);

    public void unSubscribe(SubscriptionRequestDTO subscription);

    public void sendHelpMessage(SubscriptionRequestDTO subscription);

    public void process(SubscriptionRequestDTO request);

}
