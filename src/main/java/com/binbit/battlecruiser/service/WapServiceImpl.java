package com.binbit.battlecruiser.service;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.binbit.battlecruiser.dto.SubscriptionCancelDTO;
import com.binbit.battlecruiser.dto.SubscriptionDTO;
import com.binbit.battlecruiser.dto.SubscriptionStatusDTO;
import com.binbit.battlecruiser.dto.kannel.KannelMessageDTO;
import com.binbit.battlecruiser.dto.promos.WapConfirmPinRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapResponseBuilder;
import com.binbit.battlecruiser.dto.promos.WapResponseDTO;
import com.binbit.battlecruiser.dto.promos.WapSendPinRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapSubscribeRequestDTO;
import com.binbit.battlecruiser.dto.promos.WapUnsubscribeRequestDTO;
import com.binbit.battlecruiser.model.Attempt;
import com.binbit.battlecruiser.service.BillService.BillStatus;
import com.binbit.battlecruiser.service.model.AttemptService;
import com.binbit.battlecruiser.service.model.AttemptService.AttemptType;
import com.binbit.battlecruiser.service.model.ConfigurationService;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class WapServiceImpl implements WapService {


    /** WapServiceImpl's log. */
    private static final Logger LOG = LoggerFactory.getLogger(WapServiceImpl.class);

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private AttemptService attemptService;

    @Autowired
    private KannelService kannelService;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private BillService billService;

    @Override
    public WapResponseDTO saveAttemptValidateSubscribeAndBill(WapSubscribeRequestDTO request) {
        try {
            //Save Attempt
            final Attempt attempt = saveAttempt(request, AttemptType.WAP_SUBSCRIBE);
            //Validate
            if (subscriptionService.isAlreadySubscribed(request.getServiceId(), request.getMsisdn())) {
                final WapResponseBuilder builder = new WapResponseBuilder();
                builder.setSubscriptionAndBillCode(SubscriptionService.CODE_USER_ALREADY_ACTIVE)
                .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_USER_ALREADY_ACTIVE);
                return builder.build();
            } else {
                //Subscribe
                final SubscriptionDTO subscription = new SubscriptionDTO();
                subscription.setServiceId(request.getServiceId());
                subscription.setMsisdn(request.getMsisdn());
                final SubscriptionStatusDTO subscriptionEstatus = subscriptionService.subscribe(subscription);
                //TODO validar si se le debe mandar mensaje al usuario

                /*final KannelMessageDTO message = new KannelMessageDTO();
                message.setFrom(configurationService.getFreeFrom(request.getServiceId()));
                message.setTo(request.getMsisdn());
                message.setText(configurationService.getWelcomeMessage(request.getServiceId()));
                kannelService.sendSms(message);*/

                final BillStatus billStatus = billService.bill(request.getServiceId(), request.getMsisdn(), attempt.getId());
                if (BillStatus.QUEUED.equals(billStatus)) {
                    final WapResponseBuilder builder = new WapResponseBuilder();
                    builder.setSubscriptionCode(SubscriptionService.CODE_SUCCESS)
                    .setSubscriptionMessage(SubscriptionService.MESSAGE_SUCCESS)
                    .setBillCode(SubscriptionService.CODE_IN_PROCESS)
                    .setBillMessage(SubscriptionService.MESSAGE_IN_PROCESS);
                    return builder.build();
                } else if (BillStatus.BILLED.equals(billStatus)) {
                    final WapResponseBuilder builder = new WapResponseBuilder();
                    builder.setSubscriptionAndBillCode(SubscriptionService.CODE_SUCCESS)
                    .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_SUCCESS);
                } else if (BillStatus.REJECTED.equals(billStatus)){
                    final WapResponseBuilder builder = new WapResponseBuilder();
                    builder.setSubscriptionCode(SubscriptionService.CODE_SUCCESS)
                    .setSubscriptionMessage(SubscriptionService.MESSAGE_SUCCESS)
                    .setBillCode(SubscriptionService.CODE_REJECTED)
                    .setBillMessage(SubscriptionService.MESSAGE_REJECTED);
                } else {
                    LOG.info("-- saveAttemptValidateSubscribeAndBill() billStatus[{}]", billStatus);
                    throw new NotImplementedException("billStatus " + billStatus);
                }
            }
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            final WapResponseBuilder builder = new WapResponseBuilder();
            builder.setSubscriptionAndBillCode(SubscriptionService.CODE_GENERIC_PARAMETER_ERROR)
            .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_GENERIC_PARAMETER_ERROR);
            return builder.build();
        }
        final WapResponseBuilder builder = new WapResponseBuilder();
        builder.setSubscriptionAndBillCode(SubscriptionService.CODE_GENERIC_PARAMETER_ERROR)
        .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_GENERIC_PARAMETER_ERROR);
        return builder.build();
    }



    @Override
    public WapResponseDTO unsubscribe(WapUnsubscribeRequestDTO request) {
        saveAttempt(request, AttemptType.WAP_UNSUBSCRIBE);
        //Validate
        if (subscriptionService.isAlreadySubscribed(request.getServiceId(), request.getMsisdn())) {

            final SubscriptionCancelDTO cancelDTO = new SubscriptionCancelDTO();
            cancelDTO.setServiceId(request.getServiceId());
            cancelDTO.setMsisdn(request.getMsisdn());
            //TODO revisar que parametros faltan
            subscriptionService.unsubscribe(cancelDTO);

            final KannelMessageDTO message = new KannelMessageDTO();
            message.setFrom(configurationService.getFreeFrom(request.getServiceId()));
            message.setTo(request.getMsisdn());
            message.setText(configurationService.getGoodByeMessage(request.getServiceId()));
            kannelService.sendSms(message);

            final WapResponseBuilder builder = new WapResponseBuilder();
            builder.setSubscriptionAndBillCode(SubscriptionService.CODE_USER_UNSUBSCRIBED)
            .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_USER_UNSUBSCRIBED);
            return builder.build();
        } else {
            //TODO como responder a esto?
            final WapResponseBuilder builder = new WapResponseBuilder();
            builder.setSubscriptionAndBillCode(SubscriptionService.CODE_USER_NOT_ACTIVE)
            .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_USER_NOT_ACTIVE);
            return builder.build();
        }

    }




    @Override
    public WapResponseDTO sendPin(WapSendPinRequestDTO request) {
        final String pin = configurationService.generatePin(request.getServiceId()) ? generatePin() : request.getPin();
        LOG.debug("sendPin() -- {}", pin);
        request.setPin(pin);
        saveAttempt(request, AttemptType.SEND_PIN);
        if (subscriptionService.isAlreadySubscribed(request.getServiceId(), request.getMsisdn())) {
            final WapResponseBuilder builder = new WapResponseBuilder();
            builder.setSubscriptionAndBillCode(SubscriptionService.CODE_USER_ALREADY_ACTIVE)
            .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_USER_ALREADY_ACTIVE);
            return builder.build();
        } else {
            final KannelMessageDTO message = new KannelMessageDTO();
            message.setFrom(configurationService.getFreeFrom(request.getServiceId()));
            message.setTo(request.getMsisdn());
            message.setText(String.format(configurationService.getPinMessage(request.getServiceId()), pin));
            kannelService.sendSms(message);
            //TODO tratamiento de errores

            //Response
            // TODO preguntarle a guillem que espera aqui
            final WapResponseBuilder builder = new WapResponseBuilder();
            builder.setSubscriptionAndBillCode(SubscriptionService.CODE_SUCCESS)
            .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_SUCCESS);
            return builder.build();
        }

    }

    private String generatePin() {
       final boolean letters = false;
       final boolean numbers = true;
       return RandomStringUtils.random(PIN_LENGTH, letters, numbers);
    }


    @Override
    public WapResponseDTO confirmPinAndSubscribe(WapConfirmPinRequestDTO request) {
        final Attempt attempt = attemptService.getByAttemptId(request.getAttemptId(), AttemptType.SEND_PIN);
        saveAttempt(request, AttemptType.CONFIRM_PIN);
        if (request.getPin().equals(attempt.getPin())) {
            final WapSubscribeRequestDTO param = new WapSubscribeRequestDTO();
            BeanUtils.copyProperties(request, param);
            return saveAttemptValidateSubscribeAndBill(param);
        } else {
            final WapResponseBuilder builder = new WapResponseBuilder();
            builder.setSubscriptionAndBillCode(SubscriptionService.CODE_INVALID_PIN)
            .setSubscriptionAndBillMessage(SubscriptionService.MESSAGE_INVALID_PIN);
            return builder.build();
        }
    }

    public Attempt saveAttempt(WapRequestDTO request, AttemptType type) {
        final Attempt attempt = new Attempt();
        attempt.setAttemptId(request.getAttemptId());
        attempt.setMsisdn(request.getMsisdn());
        attempt.setPromoId(request.getPromoId());
        attempt.setServiceId(request.getServiceId());
        attempt.setCarrierId(request.getCarrierId());
        attempt.setServiceTypeId(request.getServiceTypeId());
        attempt.setChannelId(request.getChannelId());
        attempt.setPin(request.getPin());
        attempt.setAttemptTypeId(type.id());
        return attemptService.insert(attempt);
    }

}
