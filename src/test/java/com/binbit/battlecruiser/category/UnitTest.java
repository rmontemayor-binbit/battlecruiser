package com.binbit.battlecruiser.category;

/**
 * Category for test that only requires java to run.
 *
 * @author rmontemayor
 *
 */
public interface UnitTest {

}
