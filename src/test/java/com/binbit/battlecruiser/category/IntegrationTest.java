package com.binbit.battlecruiser.category;

/**
 *
 * Category for test that requires webserver or database to run.
 *
 * @author rmontemayor
 *
 */
public interface IntegrationTest {

}
