package com.binbit.battlecruiser.controller.promos;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.binbit.battlecruiser.category.IntegrationTest;

@SpringBootTest
@Category(IntegrationTest.class)
@RunWith(SpringRunner.class)
public class PromosWapControllerIntegrationTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testSubscribe() throws Exception {
        mockMvc.perform(get("/wap/subscribe"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", not(nullValue())))
            .andExpect(jsonPath("$.subscription", not(nullValue())))
            .andExpect(jsonPath("$.subscription.code", not(nullValue())))
            .andExpect(jsonPath("$.subscription.message", not(nullValue())))
            .andExpect(jsonPath("$.bill.code", not(nullValue())))
            .andExpect(jsonPath("$.bill.message", not(nullValue())))
            .andExpect(jsonPath("$.redirection", not(nullValue())));
    }

    @Test
    public void testSendPin() throws Exception {
        mockMvc.perform(get("/wap/send_pin"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$", not(nullValue())))
        .andExpect(jsonPath("$.subscription", not(nullValue())))
        .andExpect(jsonPath("$.subscription.code", not(nullValue())))
        .andExpect(jsonPath("$.subscription.message", not(nullValue())))
        .andExpect(jsonPath("$.bill.code", not(nullValue())))
        .andExpect(jsonPath("$.bill.message", not(nullValue())))
        .andExpect(jsonPath("$.redirection", nullValue()));
    }

    @Test
    public void testConfirmPin() throws Exception {
        mockMvc.perform(get("/wap/confirm_pin"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$", not(nullValue())))
        .andExpect(jsonPath("$.subscription", not(nullValue())))
        .andExpect(jsonPath("$.subscription.code", not(nullValue())))
        .andExpect(jsonPath("$.subscription.message", not(nullValue())))
        .andExpect(jsonPath("$.bill.code", not(nullValue())))
        .andExpect(jsonPath("$.bill.message", not(nullValue())))
        .andExpect(jsonPath("$.redirection", nullValue()));
    }

}
