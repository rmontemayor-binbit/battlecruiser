package com.binbit.battlecruiser.controller.kannel;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.binbit.battlecruiser.category.UnitTest;
import com.binbit.battlecruiser.dto.kannel.SmsResponseDTO;
import com.binbit.battlecruiser.service.KannelService;

@Category(UnitTest.class)
public class SmsBoxControllerTest {

	private SmsController test;

	@Before
	public void setUp() {
		test = new SmsController();
	}

	@Test
	public void testKannel() {
		final SmsResponseDTO result = test.kannel(null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null);
		assertEquals(KannelService.SUCCESS_KANNEL_RESPONSE, result);
	}
}
