package com.binbit.battlecruiser;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.binbit.battlecruiser.category.UnitTest;

@SpringBootTest
@RunWith(SpringRunner.class)
@Category(UnitTest.class)
public class AppTest {

    @Test
    public void contextLoads() {
    }

}
