package com.binbit.battlecruiser.service;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import com.binbit.battlecruiser.category.UnitTest;
import com.binbit.battlecruiser.config.properties.KannelProperties;
import com.binbit.battlecruiser.dto.kannel.KannelMessageDTO;
import com.binbit.battlecruiser.exception.unchecked.InvalidArgumentException;
import com.binbit.battlecruiser.exception.unchecked.KannelCommunicationException;

import static com.binbit.battlecruiser.test.TestConstants.PHONE_NUMBER;
import static com.binbit.battlecruiser.test.TestConstants.SMS_TEXT;
import static com.binbit.battlecruiser.test.TestConstants.HTTP_URL;

@Category(UnitTest.class)
public class KannelServiceImplTest {

    private KannelServiceImpl test;

    private KannelProperties kannelProperties;

    @Before
    public void setUp() {
        test = new KannelServiceImpl();
        kannelProperties = new KannelProperties();
        kannelProperties.setUrl(HTTP_URL);
        kannelProperties.setPassword(StringUtils.EMPTY);
        kannelProperties.setUsername(StringUtils.EMPTY);
        test.setKannelProperties(kannelProperties);
    }

    @Test
    public void testGetKannelProperties() {
        final KannelProperties properties = new KannelProperties();
        test.setKannelProperties(properties);
        assertEquals(properties, test.getKannelProperties());
    }

    @Test(expected = InvalidArgumentException.class)
    public void testSendSms_fromParameterNull() {
        final KannelMessageDTO message = new KannelMessageDTO();
        message.setFrom(null);
        test.sendSms(message);
    }

    @Test(expected = InvalidArgumentException.class)
    public void testSendSms_toParameterNull() {
        final KannelMessageDTO message = new KannelMessageDTO();
        message.setFrom(PHONE_NUMBER);
        message.setTo(null);
        test.sendSms(message);
    }

    @Test(expected = InvalidArgumentException.class)
    public void testSendSms_textParameterNull() {
        final KannelMessageDTO message = new KannelMessageDTO();
        message.setFrom(PHONE_NUMBER);
        message.setTo(PHONE_NUMBER);
        test.sendSms(message);
    }

    @Test(expected = KannelCommunicationException.class)
    public void testSendSms() {
        final KannelMessageDTO message = new KannelMessageDTO();
        message.setFrom(PHONE_NUMBER);
        message.setTo(PHONE_NUMBER);
        message.setText(SMS_TEXT);
        test.sendSms(message);
    }

}
