package com.binbit.battlecruiser.test;

public class TestConstants {

    public static final String PHONE_NUMBER = "8114114867";
    public static final String SMS_TEXT = "Hello SMS";
    public static final String HTTP_URL = "http://localhost";
}
