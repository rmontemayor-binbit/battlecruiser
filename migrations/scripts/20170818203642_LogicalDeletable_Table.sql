--// LogicalDeletable Table
CREATE TABLE battlecruiser.logical_deletable
(
     active boolean not null default true
) 
WITH (
  OIDS = FALSE
);

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.logical_deletable;
