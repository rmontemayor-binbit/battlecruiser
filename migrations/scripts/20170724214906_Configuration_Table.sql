--// Message Table
CREATE TABLE battlecruiser.configuration
(
   id bigserial, 
   code character varying(60),
   value character varying(300), 
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone, 
   active boolean not null default true,
   CONSTRAINT configuration_pk PRIMARY KEY (id),
   CONSTRAINT configuration_code_un UNIQUE (code)
) 
WITH (
  OIDS = FALSE
)
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.configuration
;
