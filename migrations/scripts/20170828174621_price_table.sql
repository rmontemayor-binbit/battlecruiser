--// price table
CREATE TABLE battlecruiser.price
(
   id bigserial, 
   value numeric(16,6) NOT NULL,
   currency_id bigint NOT NULL,
   vat numeric(16,6),
   
   CONSTRAINT price_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


ALTER TABLE battlecruiser.price
  ADD CONSTRAINT price_currency_id_fk FOREIGN KEY (currency_id) 
  REFERENCES battlecruiser.currency (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.price;


