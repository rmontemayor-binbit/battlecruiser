--// carrier service table
CREATE TABLE battlecruiser.carrier_service
(
   id bigserial, 
   carrier_id bigint NOT NULL,
   service_id bigint NOT NULL,
   
   CONSTRAINT carrier_service_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

ALTER TABLE battlecruiser.carrier_service
  ADD CONSTRAINT carrier_service_carrier_id_fk FOREIGN KEY (carrier_id) 
  REFERENCES battlecruiser.carrier (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE battlecruiser.carrier_service
  ADD CONSTRAINT carrier_service_service_id_fk FOREIGN KEY (service_id) 
  REFERENCES battlecruiser.service (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;




--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier_service;


