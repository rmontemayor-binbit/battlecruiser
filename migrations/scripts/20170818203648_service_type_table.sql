--// service type table
CREATE TABLE battlecruiser.service_type
(
   id bigserial, 
   name character varying(60) NOT NULL,
   
   CONSTRAINT service_type_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.service_type;
