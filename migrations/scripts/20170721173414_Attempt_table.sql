--// Attempt table
-- Migration SQL that makes the change goes here.
CREATE TABLE battlecruiser.attempt
(
   id bigserial, 
   attempt_id bigint, 
   ip_remote character varying(16),
   msisdn character varying(50),
   promo_id bigint, 
   publisher_id bigint, 
   service_id bigint,
   trial character varying(100),
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone, 
   active boolean not null default true,
   CONSTRAINT attempt_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.attempt
;


