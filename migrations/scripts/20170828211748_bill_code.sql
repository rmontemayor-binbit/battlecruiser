--// bill_code
CREATE TABLE battlecruiser.bill_code
(
   id bigserial, 
   code character varying (60) NOT NULL,
   percentage int NOT NULL,
   
   CONSTRAINT bill_code_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.bill_code;