--// shortcode table
CREATE TABLE battlecruiser.shortcode
(
   id bigserial, 
   value character varying(40) NOT NULL,
   shortcode_type_id bigint NOT NULL,
   
   CONSTRAINT shortcode_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

ALTER TABLE battlecruiser.shortcode
  ADD CONSTRAINT shortcode_shortcode_type_id_fk FOREIGN KEY (shortcode_type_id) 
  REFERENCES battlecruiser.shortcode_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.shortcode;


