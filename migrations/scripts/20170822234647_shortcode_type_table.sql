--// shortcode type table
CREATE TABLE battlecruiser.shortcode_type
(
   id bigserial, 
   name character varying(60) NOT NULL,
   
   CONSTRAINT shortcode_type_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.shortcode_type;
