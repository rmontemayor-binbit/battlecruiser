--// Change Attempt table rename type column to typeId
ALTER TABLE battlecruiser.attempt
   DROP COLUMN type
;
   
ALTER TABLE battlecruiser.attempt
   ADD COLUMN attempt_type_id bigint
;
   
ALTER TABLE battlecruiser.attempt
  ADD CONSTRAINT attempt_attempt_type_fk FOREIGN KEY (attempt_type_id) 
  REFERENCES battlecruiser.attempt_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

CREATE INDEX attempt_attempt_type_id_idx
   ON battlecruiser.attempt (attempt_type_id ASC NULLS LAST)
;




--//@UNDO
-- SQL to undo the change goes here.
ALTER TABLE battlecruiser.attempt
   DROP COLUMN attempt_type_id
;

ALTER TABLE battlecruiser.attempt
   ADD COLUMN type character varying (20)
;