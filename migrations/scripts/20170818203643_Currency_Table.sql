--// Currency Table
CREATE TABLE battlecruiser.currency
(
   id bigserial, 
   name character varying(40) NOT NULL,
   code character varying(3) NOT NULL,

   CONSTRAINT currency_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


CREATE INDEX currency_code_idx
   ON battlecruiser.currency (code ASC NULLS LAST);

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.currency;

