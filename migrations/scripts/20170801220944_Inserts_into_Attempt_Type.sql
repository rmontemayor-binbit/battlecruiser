--// Inserts into Attempt_Type
-- Migration SQL that makes the change goes here.
insert into battlecruiser.attempt_type
(id, name, description, created_by, created_date, updated_by, updated_date) VALUES
(10, 'SMS_SUBSCRIBE', 'Subscription by sms', 'SYSTEM', now(), 'SYSTEM', now())
,(20, 'WAP_SUBSCRIBE', 'Subscribe by wap', 'SYSTEM', now(), 'SYSTEM', now())
,(30, 'SMS_UNSUBSCRIBE', 'Unsubscription by sms', 'SYSTEM', now(), 'SYSTEM', now())
,(40, 'WAP_UNSUBSCRIBE', 'Unsubscription by wap', 'SYSTEM', now(), 'SYSTEM', now())
,(50, 'SEND_PIN', 'Send Confirmation Pin', 'SYSTEM', now(), 'SYSTEM', now())
,(60, 'CONFIRM_PIN', 'Confirm Pin', 'SYSTEM', now(), 'SYSTEM', now())
;

commit;
--//@UNDO
truncate  table battlecruiser.attempt_type cascade
;
commit;

