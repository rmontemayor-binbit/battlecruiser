--// carrier service keyword table 
CREATE TABLE battlecruiser.carrier_service_keyword
(
   id bigserial, 
   carrier_service_id bigint NOT NULL,
   keyword_id bigint NOT NULL,

   CONSTRAINT carrier_service_keyword_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


ALTER TABLE battlecruiser.carrier_service_keyword
  ADD CONSTRAINT carrier_service_keyword_carrier_service_id_fk FOREIGN KEY (carrier_service_id) 
  REFERENCES battlecruiser.carrier_service (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE battlecruiser.carrier_service_keyword
  ADD CONSTRAINT carrier_service_keyword_keyword_id_fk FOREIGN KEY (keyword_id) 
  REFERENCES battlecruiser.keyword (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier_service_keyword;

