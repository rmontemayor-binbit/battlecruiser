--// Carrier Table
CREATE TABLE battlecruiser.carrier
(
   id bigserial, 
   name character varying(30) NOT NULL,
   country_id bigint NOT NULL,
   mobile_network_code character varying(3) NOT NULL,
   smsc_id character varying(30) NOT NULL,

   CONSTRAINT carrier_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


CREATE INDEX carrier_country_idx
   ON battlecruiser.country (country_id ASC NULLS LAST);

CREATE INDEX carrier_smsc_id_idx
   ON battlecruiser.country (smsc_id ASC NULLS LAST);

ALTER TABLE battlecruiser.carrier
  ADD CONSTRAINT carrier_country_id_fk FOREIGN KEY (country_id) 
  REFERENCES battlecruiser.country (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE battlecruiser.carrier
add CONSTRAINT carrier_smsc_id_uq unique (smsc_id);

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier;

