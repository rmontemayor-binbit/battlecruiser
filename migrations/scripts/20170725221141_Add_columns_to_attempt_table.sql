--// Add columns to attempt table
ALTER TABLE battlecruiser.attempt
   ADD COLUMN carrier_id bigint;

ALTER TABLE battlecruiser.attempt
   ADD COLUMN service_type_id bigint;

ALTER TABLE battlecruiser.attempt
   ADD COLUMN channel_id bigint;

ALTER TABLE battlecruiser.attempt
   ADD COLUMN pin character varying (10);

ALTER TABLE battlecruiser.attempt
   ADD COLUMN type character varying (20);

CREATE INDEX attempt_attempt_id_idx
   ON battlecruiser.attempt (attempt_id ASC NULLS LAST);




--//@UNDO
ALTER TABLE battlecruiser.attempt
   DROP COLUMN carrier_id;

ALTER TABLE battlecruiser.attempt
   DROP COLUMN service_type_id;

ALTER TABLE battlecruiser.attempt
   DROP COLUMN channel_id;

ALTER TABLE battlecruiser.attempt
   DROP COLUMN pin;

ALTER TABLE battlecruiser.attempt
   DROP COLUMN type;

DROP INDEX battlecruiser.attempt_attempt_id_idx;


