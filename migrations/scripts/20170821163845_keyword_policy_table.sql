--// keyword_policy table
CREATE TABLE battlecruiser.keyword_policy
(
   id bigserial, 
   name character varying(60) NOT NULL,
   
   CONSTRAINT keyword_policy_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.keyword_policy;
