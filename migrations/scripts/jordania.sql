INSERT INTO currency(
            created_by, created_date, updated_by, updated_date, active, 
            name, code)
    VALUES ('rmonper', now(), 'rmonper', now(), true,  
            'Dinar', 'JOD');

INSERT INTO country(
            created_by, created_date, updated_by, updated_date, active, 
            name, iso_code, iso_code_3, numeric_code, phone_code, mobile_country_code, 
            currency_id, default_time_zone)
    VALUES ('rmonper', now(), 'rmonper', now(), true, 
            'Jordan', 'JO', 'JOR', 400, '+962', '416', 
            (select id from battlecruiser.currency where code = 'JOD'), 'UTC+3');

 INSERT INTO carrier(
            created_by, created_date, updated_by, updated_date, active,
            id, name, country_id, mobile_network_code, smsc_id)
    VALUES ('rmonper', now(), 'rmonper', now(), true,
            41677, 'Orange/Petra', (select id from battlecruiser.country where name = 'Jordan') , '77', 'JOR_Orange');


--// Esto puede cambiar


insert into shortcode
(value, shortcode_type_id)
values 
(
'97735', (select id from shortcode_type where name = 'PAY')
);



insert into shortcode
(value, shortcode_type_id)
values 
(
'962777090485', (select id from shortcode_type where name = 'FREE')
);


--// TODO de aqui solo el nombre es correcto
insert into service
(service_id, name, service_type_id);
values
(90485, 'BitPlay English', 1);

insert into service
(service_id, name, service_type_id)
values
(90485, 'BitPlay Arabic', 1);


insert into keyword
(value, keyword_type_id, keyword_policy_id)
values
('ALTA' ,(select id from keyword_type where name = 'OPTIN'), (select id from keyword_policy where name = 'EXACT_MATCH'));


insert into carrier_service
(carrier_id, service_id)
values
((select id from carrier where smsc_id = 'JOR_Orange' ),(select id from service where name = 'BitPlay English'));

insert into carrier_service
(carrier_id, service_id)
values
((select id from carrier where smsc_id = 'JOR_Orange' ),(select id from service where name = 'BitPlay Arabic'));



insert into carrier_service_keyword
(carrier_service_id, keyword_id)
values
(2, 4);

insert into carrier_service_shortcode
(carrier_service_id, shortcode_id)
values
(2, 6);

insert into carrier_service_shortcode
(carrier_service_id, shortcode_id)
values
(2, 5);