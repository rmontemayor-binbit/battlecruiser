--// brand
CREATE TABLE battlecruiser.brand
(
   id bigserial, 
   name character varying(60) NOT NULL,
   
   CONSTRAINT brand_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.brand;


