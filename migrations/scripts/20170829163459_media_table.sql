--// media table
CREATE TABLE battlecruiser.media
(
   id bigserial, 
   media_id bigint NOT NULL,
   name character varying (60),
   carrier_service_keyword_id bigint NOT NULL,
   carrier_service_shortcode_id bigint NOT NULL,
   
   CONSTRAINT media_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

ALTER TABLE battlecruiser.media
ADD CONSTRAINT carrier_service_keyword_shortcode_uk UNIQUE (carrier_service_keyword_id, carrier_service_shortcode_id );


ALTER TABLE battlecruiser.media
  ADD CONSTRAINT media_carrier_service_keyword_id_fk FOREIGN KEY (carrier_service_keyword_id) 
  REFERENCES battlecruiser.carrier_service_keyword (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE battlecruiser.media
  ADD CONSTRAINT media_carrier_service_shortcode_id_fk FOREIGN KEY (carrier_service_shortcode_id) 
  REFERENCES battlecruiser.carrier_service_shortcode (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.media;
