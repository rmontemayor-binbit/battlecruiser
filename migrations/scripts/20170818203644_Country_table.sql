--// Country table
CREATE TABLE battlecruiser.country
(
   id bigserial, 
   name character varying(40) NOT NULL,
   iso_code char(2) NOT NULL,
   iso_code_3 char(3) NOT NULL,
   numeric_code numeric(3) NOT NULL,
   phone_code numeric(5) NOT NULL,
   mobile_country_code character varying(3) NOT NULL,
   currency_id bigint NOT NULL,
   default_time_zone character varying (10) NOT NULL,
   
   CONSTRAINT country_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


CREATE INDEX country_iso_code_idx
   ON battlecruiser.country (iso_code ASC NULLS LAST);

CREATE INDEX country_iso_code_3_idx
   ON battlecruiser.country (iso_code_3 ASC NULLS LAST);

CREATE INDEX country_numeric_code_idx
   ON battlecruiser.country (numeric_code ASC NULLS LAST);

ALTER TABLE battlecruiser.country
  ADD CONSTRAINT country_currency_id_fk FOREIGN KEY (currency_id) 
  REFERENCES battlecruiser.currency (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.country;

