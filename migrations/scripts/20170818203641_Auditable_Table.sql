--// Auditable Table
CREATE TABLE battlecruiser.auditable
(
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone
) 
WITH (
  OIDS = FALSE
)
;

alter table auditable alter column created_by set default 'rmonper';
alter table auditable alter column updated_by set default 'rmonper';

alter table auditable alter column updated_date set default now();
alter table auditable alter column created_date set default now();



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.auditable;


