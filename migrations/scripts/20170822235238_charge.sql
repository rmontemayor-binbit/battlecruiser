--// charge
CREATE TABLE battlecruiser.charge
(
   id bigserial, 
   
   CONSTRAINT charge_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.charge;