--// alter table keyword remove column carrier_id service_id
ALTER TABLE battlecruiser.keyword DROP column carrier_id;
ALTER TABLE battlecruiser.keyword DROP column service_id;

ALTER TABLE battlecruiser.keyword rename column name to value;


--//@UNDO
ALTER TABLE battlecruiser.keyword rename column value to name;



