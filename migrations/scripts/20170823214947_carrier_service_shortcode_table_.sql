--// carrier_service_shortcode table 
CREATE TABLE battlecruiser.carrier_service_shortcode
(
   id bigserial, 
   carrier_service_id bigint NOT NULL,
   shortcode_id bigint NOT NULL,

   CONSTRAINT carrier_service_shortcode_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


ALTER TABLE battlecruiser.carrier_service_shortcode
  ADD CONSTRAINT carrier_service_shortcode_carrier_service_id_fk FOREIGN KEY (carrier_service_id) 
  REFERENCES battlecruiser.carrier_service (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE battlecruiser.carrier_service_shortcode
  ADD CONSTRAINT carrier_service_shortcode_shortcode_id_fk FOREIGN KEY (shortcode_id) 
  REFERENCES battlecruiser.shortcode (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier_service_shortcode;

