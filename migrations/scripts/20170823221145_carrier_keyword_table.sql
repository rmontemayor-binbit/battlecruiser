--// carrier_shortcode table
CREATE TABLE battlecruiser.carrier_keyword
(
   id bigserial, 
   carrier_id bigint NOT NULL,
   keyword_id bigint NOT NULL,

   CONSTRAINT carrier_keyword_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


ALTER TABLE battlecruiser.carrier_keyword
  ADD CONSTRAINT carrier_keyword_carrier_id_fk FOREIGN KEY (carrier_id) 
  REFERENCES battlecruiser.carrier (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE battlecruiser.carrier_keyword
  ADD CONSTRAINT carrier_keyword_keyword_id_fk FOREIGN KEY (keyword_id) 
  REFERENCES battlecruiser.keyword (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier_keyword;
