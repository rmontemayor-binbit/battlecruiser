--// Subscription table
-- Migration SQL that makes the change goes here.
CREATE TABLE battlecruiser.subscription
(
   id bigserial, 
   service_id bigint, 
   customer_id bigint,
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone, 
   active boolean not null default true,
   CONSTRAINT subscription_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;

ALTER TABLE battlecruiser.subscription
  ADD CONSTRAINT subscription_service_fk FOREIGN KEY (service_id) 
  REFERENCES battlecruiser.service (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE battlecruiser.subscription
  ADD CONSTRAINT subscription_customer_fk FOREIGN KEY (customer_id) 
  REFERENCES battlecruiser.customer (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.subscription;


