--// message
CREATE TABLE battlecruiser.message
(
   id bigserial, 
   text character varying(200) NOT NULL,
   
   CONSTRAINT message_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.message;
