--// Service Table
-- Migration SQL that makes the change goes here.
CREATE TABLE battlecruiser.service
(
   id bigserial, 
   service_id bigint, 
   name character varying(40) NOT NULL,
   service_type_id bigint NOT NOT NULL,
   carrier_id bigint,
   
   CONSTRAINT service_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

CREATE INDEX service_service_id_idx
   ON battlecruiser.service (service_id ASC NULLS LAST);

ALTER TABLE battlecruiser.service
  ADD CONSTRAINT service_service_type_id_fk FOREIGN KEY (service_type_id) 
  REFERENCES battlecruiser.service_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE battlecruiser.service
  ADD CONSTRAINT service_carrier_id_fk FOREIGN KEY (carrier_id) 
  REFERENCES battlecruiser.carrier (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.service;


