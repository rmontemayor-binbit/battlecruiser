--// Drop tables carrier customer service subscription
-- Migration SQL that makes the change goes here.
DROP TABLE IF EXISTS battlecruiser.subscription;
DROP TABLE IF EXISTS battlecruiser.service;
DROP TABLE IF EXISTS battlecruiser.customer;
DROP TABLE IF EXISTS battlecruiser.carrier;



--//@UNDO
CREATE TABLE battlecruiser.carrier
(
  id bigserial NOT NULL,
  name character varying(50),
  country character varying(50),
  created_by character varying(16),
  created_date timestamp with time zone,
  updated_by character varying(16),
  updated_date timestamp with time zone,
  active boolean NOT NULL DEFAULT true,
  CONSTRAINT carrier_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE battlecruiser.customer
(
  id bigserial NOT NULL,
  msisdn character varying(50),
  created_by character varying(16),
  created_date timestamp with time zone,
  updated_by character varying(16),
  updated_date timestamp with time zone,
  active boolean NOT NULL DEFAULT true,
  CONSTRAINT customer_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE service
(
  id bigserial NOT NULL,
  name character varying(50),
  carrier_id bigint,
  created_by character varying(16),
  created_date timestamp with time zone,
  updated_by character varying(16),
  updated_date timestamp with time zone,
  active boolean NOT NULL DEFAULT true,
  CONSTRAINT service_pk PRIMARY KEY (id),
  CONSTRAINT service_carrier_fk FOREIGN KEY (carrier_id)
      REFERENCES battlecruiser.carrier (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE TABLE subscription
(
  id bigserial NOT NULL,
  service_id bigint,
  customer_id bigint,
  created_by character varying(16),
  created_date timestamp with time zone,
  updated_by character varying(16),
  updated_date timestamp with time zone,
  active boolean NOT NULL DEFAULT true,
  CONSTRAINT subscription_pk PRIMARY KEY (id),
  CONSTRAINT subscription_customer_fk FOREIGN KEY (customer_id)
      REFERENCES battlecruiser.customer (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT subscription_service_fk FOREIGN KEY (service_id)
      REFERENCES battlecruiser.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


