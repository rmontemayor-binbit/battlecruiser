--// Customer Table
-- Migration SQL that makes the change goes here.
CREATE TABLE battlecruiser.customer
(
   id bigserial, 
   msisdn character varying(50), 
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone, 
   active boolean not null default true,
   CONSTRAINT customer_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.customer;


