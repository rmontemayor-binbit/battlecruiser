--// Configuration default values
INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].welcome', 'welcome', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].bill', 'first bill', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].freeFrom', '60700', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].billFrom', '60701', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].kannelDlrUrl', 'http://localhost:8080/sms/dlr', 'SYSTEM', now(), 'SYSTEM', now(), true);


INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].goodBye', 'optout', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].generatePin', 'true', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].pinMessage', 'Your pin is %s', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].alreadySubscribedMessage', 'You''r already subscribed.', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].notSubscribedMessage', 'You aren''t subscribed.', 'SYSTEM', now(), 'SYSTEM', now(), true);


INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('apiCoreUrl', 'http://localhost:8060/api-core', 'SYSTEM', now(), 'SYSTEM', now(), true);


INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('smsc[uy_movistar].shortCode[60700].serviceId', '1', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].subscribeCodes', 'ALTA', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].unsubscribeCodes', 'BAJA', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[0].helpMessage', 'This is a default help message.', 'SYSTEM', now(), 'SYSTEM', now(), true);

INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[1].helpMessage', 'This is a help message.', 'SYSTEM', now(), 'SYSTEM', now(), true);


INSERT INTO battlecruiser.configuration 
(code, value, created_by, created_date, updated_by, updated_date, active)
VALUES
('service[0].freeFrom', '60700', 'SYSTEM', now(), 'SYSTEM', now(), true);

COMMIT;

--//@UNDO
TRUNCATE battlecruiser.configuration;


