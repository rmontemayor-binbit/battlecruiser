INSERT INTO battlecruiser.currency(
            created_by, created_date, updated_by, updated_date, active, 
            name, code)
    VALUES ('rmonper', now(), 'rmonper', now(), true,  
            'Philippine Peso', 'PHP');


INSERT INTO battlecruiser.country(
            created_by, created_date, updated_by, updated_date, active, 
            name, iso_code, iso_code_3, numeric_code, phone_code, mobile_country_code, 
            currency_id, default_time_zone)
    VALUES ('rmonper', now(), 'rmonper', now(), true, 
            'Philippines', 'PH', 'PHL', 608, '+63', '515', 
            (select id from battlecruiser.currency where code = 'PHP'), 'UTC+8');



INSERT INTO battlecruiser.carrier(
            created_by, created_date, updated_by, updated_date, active,
            name, country_id, mobile_network_code)
    VALUES ('rmonper', now(), 'rmonper', now(), true,
            'sun', (select id from battlecruiser.country where name = 'Philippines') , '05');


INSERT INTO keyword_type(
            created_by, created_date, updated_by, updated_date, active, 
            name)
    VALUES ('rmonper', now(), 'rmonper', now(), true, 'ALTA');


INSERT INTO keyword(
            created_by, created_date, updated_by, updated_date, active,
            name, keyword_type_id, carrier_id, service_id)
    VALUES ('rmonper', now(), 'rmonper', now(), true, 
            'ALTA', (select id from battlecruiser.keyword_type where name = 'ALTA'), 
            (select id from battlecruiser.carrier where name = 'sun'), null);


insert into service_type
(created_by, created_date, updated_by, updated_date, active,
name)
values 
('rmonper', now(), 'rmonper', now(), true,
'ALL_YOU_CAN_EAT');

insert into service
(created_by, created_date, updated_by, updated_date, active,
service_id, name, service_type_id, carrier_id)
values 
('rmonper', now(), 'rmonper', now(), true,
1, 'BARCELONA', (select id from service_type where name = 'ALL_YOU_CAN_EAT'),
(select id from carrier where name = 'sun' and country_id = (select id from country where name = 'Philippines'))
);


insert into carrier_service
(created_by, created_date, updated_by, updated_date, active,
service_id, carrier_id)
values 
('rmonper', now(), 'rmonper', now(), true,
(select id from service where name = 'BARCELONA'),
(select id from carrier where name = 'sun' and country_id = (select id from country where name = 'Philippines'))
);

insert into keyword_type
(created_by, created_date, updated_by, updated_date, active,
name)
values 
('rmonper', now(), 'rmonper', now(), true,
'EXACT_MATCH'
);

insert into keyword
(created_by, created_date, updated_by, updated_date, active,
value, keyword_type_id)
values 
('rmonper', now(), 'rmonper', now(), true,
'ALTA', (select id from keyword_type where name = 'EXACT_MATCH')
);

insert into carrier_service_keyword
(created_by, created_date, updated_by, updated_date, active,
carrier_service_id, keyword_id)
values 
('rmonper', now(), 'rmonper', now(), true,
(select id from carrier_service where carrier_id = (select id from carrier where name = 'sun') and service_id = (select id from service where name = 'BARCELONA')),
(select id from keyword where value = 'ALTA')
);


insert into shortcode_type
(created_by, created_date, updated_by, updated_date, active,
id, name)
values 
('rmonper', now(), 'rmonper', now(), true,
10, 'OPTIN'
);

insert into shortcode
(created_by, created_date, updated_by, updated_date, active,
value, shortcode_type_id)
values 
('rmonper', now(), 'rmonper', now(), true,
'60700', 10
);

insert into carrier_service_shortcode
(created_by, created_date, updated_by, updated_date, active,
 carrier_service_id, shortcode_id)
values 
('rmonper', now(), 'rmonper', now(), true,
1, 3
);

insert into keyword_policy
(created_by, created_date, updated_by, updated_date, active,
id, name)
values 
('rmonper', now(), 'rmonper', now(), true,
10, 'EXACT_MATCH'
);


insert into keyword_policy
(created_by, created_date, updated_by, updated_date, active,
id, name)
values 
('rmonper', now(), 'rmonper', now(), true,
20, 'CONTAINS'
);

insert into carrier_service_keyword
(created_by, created_date, updated_by, updated_date, active,
 carrier_service_id, keyword_id)
 values
 ('rmonper', now(), 'rmonper', now(), true,
  1, 1
 );


insert into media
(created_by, created_date, updated_by, updated_date, active,
 name, media_id, carrier_service_id, carrier_service_keyword_id, carrier_service_shortcode_id)
 values
 ('rmonper', now(), 'rmonper', now(), true,
  'Periodico', 5, 1, 2, 1
 );

 insert into shortcode_type
(id, name)
values
(20, 'PAY')


