--// carrier_service_bill_policy
CREATE TABLE battlecruiser.carrier_service_bill_policy
(
   id bigserial, 
   carrier_service_id bigint NOT NULL,
   bill_policy_id bigint NOT NULL,
   
   CONSTRAINT carrier_service_bill_policy_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


ALTER TABLE battlecruiser.carrier_service_bill_policy
  ADD CONSTRAINT carrier_service_bill_policy_carrier_service_id_fk FOREIGN KEY (carrier_service_id) 
  REFERENCES battlecruiser.carrier_service (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE battlecruiser.carrier_service_bill_policy
  ADD CONSTRAINT carrier_service_bill_policy_bill_policy_id_fk FOREIGN KEY (bill_policy_id) 
  REFERENCES battlecruiser.bill_policy (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE battlecruiser.carrier_service_bill_policy 
ADD CONSTRAINT carrier_service_bill_policy_uc UNIQUE (carrier_service_id, bill_policy_id);



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier_service_bill_policy;