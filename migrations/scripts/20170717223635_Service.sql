--// Service
-- Migration SQL that makes the change goes here.
CREATE TABLE battlecruiser.service
(
   id bigserial, 
   name character varying(50), 
   carrier_id bigint, 
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone, 
   active boolean not null default true,
   CONSTRAINT service_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;

ALTER TABLE service
  ADD CONSTRAINT service_carrier_fk FOREIGN KEY (carrier_id) 
  REFERENCES battlecruiser.carrier (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.service;


