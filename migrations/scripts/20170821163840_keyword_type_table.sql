--// keyword_type table
CREATE TABLE battlecruiser.keyword_type
(
   id bigserial, 
   name character varying(60) NOT NULL,
   
   CONSTRAINT keyword_type_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;

--//@UNDO
DROP TABLE IF EXISTS battlecruiser.keyword_type;
