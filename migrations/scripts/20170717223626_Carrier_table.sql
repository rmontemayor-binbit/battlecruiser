--// Carrier table
-- Migration SQL that makes the change goes here.
CREATE TABLE battlecruiser.carrier
(
   id bigserial, 
   name character varying(50), 
   country character varying(50), 
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone, 
   active boolean not null default true,
   CONSTRAINT carrier_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier;


