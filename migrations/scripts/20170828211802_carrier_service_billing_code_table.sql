--// carrier_service_bill_code table
CREATE TABLE battlecruiser.carrier_service_bill_code
(
   id bigserial, 
   carrier_service_id bigint NOT NULL,
   bill_code_id bigint NOT NULL,
   
   CONSTRAINT carrier_service_bill_code_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


ALTER TABLE battlecruiser.carrier_service_bill_code
  ADD CONSTRAINT carrier_service_bill_code_carrier_service_id_fk FOREIGN KEY (carrier_service_id) 
  REFERENCES battlecruiser.carrier_service (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;


ALTER TABLE battlecruiser.carrier_service_bill_code
  ADD CONSTRAINT carrier_service_bill_code_bill_code_id_fk FOREIGN KEY (bill_code_id) 
  REFERENCES battlecruiser.bill_code (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;




--//@UNDO
DROP TABLE IF EXISTS battlecruiser.carrier_service_bill_code;