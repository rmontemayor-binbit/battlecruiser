--// language table 
CREATE TABLE battlecruiser.language
(
   id bigserial, 
   name character varying(60) NOT NULL,
   code character varying(6) NOT NULL,
   
   CONSTRAINT shortcode_type_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.language;


