--// bill_policy table
CREATE TABLE battlecruiser.bill_policy
(
   id bigserial, 
   periodicity int NOT NULL,
   partial_bill boolean NOT NULL,
   retry boolean NOT NULL,
   retries_in_a_day int,
   
   CONSTRAINT bill_policy_pk PRIMARY KEY (id)
)
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.bill_policy;