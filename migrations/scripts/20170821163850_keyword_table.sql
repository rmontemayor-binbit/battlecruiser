--// keyword table
CREATE TABLE battlecruiser.keyword
(
   id bigserial, 
   name character varying(60),
   keyword_type_id bigint NOT NULL,
   keyword_policy_id bigint NOT NULL,
   carrier_id bigint NOT NULL,
   service_id bigint,

   CONSTRAINT keyword_pk PRIMARY KEY (id)
) 
INHERITS (battlecruiser.auditable, battlecruiser.logical_deletable)
;


CREATE INDEX keyword_carrier_id_service_id_idx
   ON battlecruiser.keyword (carrier_id ASC NULLS LAST, service_id ASC NULLS LAST);

ALTER TABLE battlecruiser.keyword
  ADD CONSTRAINT keyword_carrier_id_fk FOREIGN KEY (carrier_id) 
  REFERENCES battlecruiser.carrier (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE battlecruiser.keyword
  ADD CONSTRAINT keyword_service_id_fk FOREIGN KEY (service_id) 
  REFERENCES battlecruiser.service (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE battlecruiser.keyword
  ADD CONSTRAINT keyword_keyword_type_id_fk FOREIGN KEY (keyword_type_id) 
  REFERENCES battlecruiser.keyword_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;

ALTER TABLE battlecruiser.keyword
  ADD CONSTRAINT keyword_keyword_policy_id_fk FOREIGN KEY (keyword_policy_id) 
  REFERENCES battlecruiser.keyword_policy (id) ON UPDATE NO ACTION ON DELETE NO ACTION
;



--//@UNDO
DROP TABLE IF EXISTS battlecruiser.keyword;


