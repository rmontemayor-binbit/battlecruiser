--// AttemptType table
CREATE TABLE battlecruiser.attempt_type
(
   id bigserial, 
   name character varying(30), 
   description character varying(200),
   created_by character varying(16), 
   created_date timestamp with time zone, 
   updated_by character varying(16), 
   updated_date timestamp with time zone, 
   active boolean not null default true,
   CONSTRAINT attempttype_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;


--//@UNDO
DROP TABLE IF EXISTS battlecruiser.attempt_type
;
